# Prusa SLA File Manager
![pipeline](https://gitlab.com/prusa3d/sl1/filemanager/badges/master/pipeline.svg)
![coverage](https://gitlab.com/prusa3d/sl1/filemanager/badges/master/coverage.svg)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

### Setup
Create a python environment for development and tests.
```bash
./scripts/setup
```

### Run Dev
```bash
./scripts/dev
```

### Run tests
```bash
./scripts/static_test # static tests
./scripts/test # united test
./scripts/test --no-browser # united test, it doesn't show the browser
```
