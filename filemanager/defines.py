# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import stat
import tempfile

from pathlib import Path

STORAGE_PATH = Path("/var/sl1fw")
PROJECT_ROOT_PATH = STORAGE_PATH / "projects"
OLD_PROJECTS_DOWNLOAD = STORAGE_PATH / "old-projects"
PROJECT_SYMLINK = STORAGE_PATH / "previous-prints"

USB_MOUNT_POINT = Path("/run/media/system")
PRINTER_MODEL_PATH = Path("/run/model")

CACHE_DIR = Path(tempfile.gettempdir()) / "projects_metadata"

PROJECT_CONFIG_FILE = "config.ini"
PROJECT_GROUP = "projects"

DEBUG_FILE = Path("/etc/debug")

PROJECT_MODE = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH

PROJECT_DIR_MODE = stat.S_IRWXU | stat.S_IRWXG | stat.S_IROTH | stat.S_IXOTH
PROJECT_DIR_CACHE = stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH

MAX_DEPTH = 256
