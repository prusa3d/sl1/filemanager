# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=C0301
# pylint: disable=missing-function-docstring
# pylint: disable=no-self-use
# pylint: disable=logging-fstring-interpolation
# pylint: disable=too-many-arguments
# pylint: disable=too-many-instance-attributes

from __future__ import annotations

import os
from logging import getLogger
from pathlib import Path
from shutil import disk_usage
from threading import Thread
from typing import Any, Dict, Optional, List

from PySignal import Signal
from pydbus.generic import signal
from requests import get as requests_get
from requests.exceptions import RequestException
from requests.models import Response

from slafw.api.decorators import (  # pylint: disable=import-error
    auto_dbus,
    dbus_api,
    last_error,
    wrap_dict_data,
    auto_dbus_signal,
)
from werkzeug.utils import secure_filename

from . import defines
from . import errors
from .manager import Manager
from .model import PrinterModel
from .transfer import Transfer

MIN_SPACE_LEFT = 110 * 1024 * 1024  # 110MB


# pylint: disable = too-many-public-methods
@dbus_api
class FileManager0:
    """Dbus api for share file system data"""

    __INTERFACE__ = "cz.prusa3d.sl1.filemanager0"

    PropertiesChanged = signal()

    @auto_dbus_signal
    def MediaInserted(self, path: str):  # pylint: disable=invalid-name
        pass

    @auto_dbus_signal
    def MediaEjected(self, root_path: str):  # pylint: disable=invalid-name
        pass

    @auto_dbus_signal
    def OneClickPrintFile(self, path: str):  # pylint: disable=invalid-name
        pass

    def __init__(self):
        self._logger = getLogger(__name__)
        self._last_exception: Optional[Exception] = None
        self._manager = Manager(PrinterModel.get_model())
        self._manager.updated.connect(self._files_update)
        self.on_changed(self._manager.current_media_path_changed, "current_media_path")
        self.on_changed(self._manager.busy_changed, "busy")
        self.on_changed(self._manager.media_present_changed, "media_present")
        self.on_changed(self._manager.media_mounted_changed, "media_mounted")
        self._manager.one_click_print_file.connect(self._one_click_print_emit)
        self._manager.media_ejected.connect(lambda path: self.MediaEjected(path))  # pylint: disable=unnecessary-lambda
        # pylint: disable=unnecessary-lambda
        self._manager.media_inserted.connect(lambda path: self.MediaInserted(path))

        # download mgr
        self._download_thread: Thread = None
        self._transfer: Transfer = None

    def on_changed(self, changed_signal: Signal, property_name: str):
        """
        Connect signal to property change event

        Args:
        `changed_signal` is the signal that will be connected to the property change event
        """
        changed_signal.connect(lambda val: FileManager0.report_change(self, property_name, val))

    def report_change(self, property_name: str, value: Any):
        """
        Universal method to report changes to a property. Use on_changed to bind the
        property name and use the value passed by the appropriate signal
        """
        self._logger.debug("PropertyChanged: %s: %s", property_name, str(value))
        self.PropertiesChanged(
            self.__INTERFACE__,
            {property_name: value},
            [],
        )

    @auto_dbus  # type: ignore
    @property
    def media_present(self) -> bool:
        """
        Is the USB drive inserted?
        """
        return self._manager.media_present

    @auto_dbus  # type: ignore
    @property
    def media_mounted(self) -> bool:
        """
        Is the USB drive mounted?
        """
        return self._manager.media_mounted

    @auto_dbus
    def media_umount(self) -> bool:
        return self._manager.umount_usb(Path(self.current_media_path))

    @auto_dbus
    def media_mount(self) -> bool:
        return self._manager.mount_usb()

    @auto_dbus  # type: ignore
    def start_transfer(
        self,
        storage: str,
        path: str,
        size: int,
        transfer_type: str,
        to_print: bool,
        start_cmd_id: int,
    ) -> int:
        self._transfer = Transfer(storage, path, size, transfer_type, to_print, start_cmd_id)
        return self._transfer.id

    @auto_dbus  # type: ignore
    @last_error
    def update_transfer_progress(self, chunk_size: int):
        if self._transfer:
            self._transfer.update_progress(chunk_size)

    @auto_dbus  # type: ignore
    @property
    def transfer(self) -> Dict[str, Any]:
        if self._transfer:
            t = self._transfer.__dict__
            return wrap_dict_data(t)
        return {}

    @auto_dbus  # type: ignore
    def stop_transfer(self):
        file_path = Path(self._transfer.path)
        to_storage = self._manager.path_to_storage(file_path)
        self._manager.fsm.ch_mode_owner(file_path, defines.PROJECT_GROUP, defines.PROJECT_MODE)
        to_storage.cache.remove(file_path)
        to_storage.cache.add(file_path)
        if self._transfer.to_print:
            for storage in self._manager.storages:
                if self._transfer.storage == storage.cache.name:
                    self._manager.one_click_print_file.emit(Path(storage.cache.newest_file.path))
        self._transfer = None

    @auto_dbus  # type: ignore
    def download_from_url(
        self,
        url: str,
        storage: str,
        path: str,
        transfer_type: str,
        to_print: bool,
        headers: Dict[str, str],  # needed by Connect. Otherwise you get 401
        start_cmd_id: int,
    ) -> int:
        if self._download_thread and self._download_thread.is_alive():
            raise errors.FileAlreadyExists("Uploading in progress")

        default_headers = {
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.5",
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": "1",
        }

        headers.update(default_headers)

        try:
            req = requests_get(url, stream=True, allow_redirects=True, timeout=10, headers=headers)

            if req.status_code >= 400:
                raise errors.FileNotFound(str(req.status_code) + ": " + req.reason)
            file_name = path.split("/")[-1]
            file_path = os.path.dirname(path)
            file_size = int(req.headers.get("Content-Length", 0))
            self._logger.debug(f"DOWNLOAD: url {url},  file_name {file_name}")
            file_path = self.validate_project(file_name, storage, file_path, file_size)

            self._logger.debug(f"DOWNLOAD: file_size {file_size}, file_path {file_path}")

            transfer_id = self.start_transfer(
                storage,
                file_path,
                file_size,
                transfer_type,
                to_print,
                start_cmd_id,
            )

            self._download_thread = Thread(
                target=self._download_from_url,
                args=(req, file_path),
                daemon=True,
            )
            self._download_thread.start()

            return transfer_id
        except RequestException as esc:
            raise errors.ProjectErrorCantRead() from esc

    def _download_from_url(self, req: Response, path: str):
        self._logger.debug(f"DOWNLOAD: destination {path}")

        with open(path, "wb") as f:
            for chunk in req.iter_content(chunk_size=2048):
                if chunk:
                    f.write(chunk)
                    self._transfer.update_progress(len(chunk))
        self.stop_transfer()

    @auto_dbus  # type: ignore
    def convert_path(self, target: str, path: str) -> str:
        for k, v in self.get_all(0).items():
            self._logger.debug(f"DOWNLOAD: convert_path({target}), {k} -> {v['path']}")
            if k == target:
                return os.path.join(v["path"], path)
        raise FileNotFoundError(target)

    @auto_dbus  # type: ignore
    def validate_project(
        self,
        filename: str,
        target: str,
        path: str,
        content_length: int,
        overwrite: bool = True,
    ) -> str:
        internal_path = self.convert_path(target, path)
        file_path = os.path.join(internal_path, secure_filename(filename))

        if filename.strip() == "":
            raise errors.FileNotFound("Invalid filename.")
        if not overwrite and os.path.exists(file_path):
            raise errors.FileAlreadyExists("Project already exists.")

        (_, extension) = os.path.splitext(file_path)
        if extension not in self.extensions:
            raise errors.InvalidExtension(f"Invalid extension: {filename}")

        os.makedirs(internal_path, exist_ok=True)
        stats = os.statvfs(internal_path)
        free = stats.f_bavail * stats.f_frsize
        if content_length + MIN_SPACE_LEFT >= free:
            raise errors.NotEnoughInternalSpace("Not enough space left on the device.")
        return file_path

    def _one_click_print_emit(self, path: Path):
        path_string = str(path)
        self._logger.debug("one_click_print_update: %s", path_string)
        self.OneClickPrintFile(path_string)

    def _files_update(self, timestamp: float):
        self._logger.debug("files_update event at %s", timestamp)
        self.PropertiesChanged(
            self.__INTERFACE__,
            {"last_update": wrap_dict_data(self._manager.last_update)},
            [],
        )

    def stop(self):
        """Stop Dbus"""

    @auto_dbus  # type: ignore
    @property
    def usb_name(self) -> str:
        return self._manager.removable_presented

    @auto_dbus  # type: ignore
    @property
    def usb_mount_point(self) -> str:
        return self._manager.current_media_path

    @auto_dbus  # type: ignore
    @property
    def busy(self) -> bool:
        return self._manager.busy

    @auto_dbus  # type: ignore
    @property
    def current_media_path(self) -> str:
        """str: Return the path to the latest media/usb flashdisk mounted. Empty string otherwise."""
        return self._manager.current_media_path

    @auto_dbus  # type: ignore
    @property
    def last_update(self) -> Dict[str, Any]:
        """float: Return the time in seconds for the last time the project files were updated."""

        return wrap_dict_data(self._manager.last_update)

    @auto_dbus  # type: ignore
    @property
    def last_exception(self) -> Dict[str, Any]:
        """Last Exception data

        Returns:
            Dict[str, Any]
        """
        return wrap_dict_data(errors.wrap_exception(self._last_exception))

    @auto_dbus  # type: ignore
    @last_error
    def get_all(self, maxdepth: int) -> Dict[str, Any]:
        """Return a dict with info all files

        sample:
        {
            "local": {
                "path": "/var/sl1fw/projects",
                "origin": "local",
                "type": "folder",
                "children": [
                {
                    "path": "/var/sl1fw/projects/examples",
                    "origin": "local",
                    "type": "folder",
                    "children": [
                    {
                        "path": "/var/sl1fw/projects/examples/Petrin_Tower_10H_50um_Prusa_Orange.sl1",
                        "origin": "local",
                        "type": "file",
                        "size": 21008282
                    },
                    {
                        "path": "/var/sl1fw/projects/examples/Calibration objects",
                        "origin": "local",
                        "type": "folder",
                        "children": [
                        {
                            "path": "/var/sl1fw/projects/examples/Calibration objects/Resin_Calibration_0.025.sl1",
                            "origin": "local",
                            "type": "file",
                            "size": 4930590
                        }
                        ]
                    }
                    ]
                }
                ]
            },
            "last_update": {
                "last_update": 1599835353,
                "local": 1599835353
            }
        }

        Returns:
            Dict[str, Any]
        """
        self._logger.debug("get_all(%d)", maxdepth)
        result = {}
        for storage in self._manager.storages:
            sources_tree = self._manager.get_files(storage, maxdepth)
            result[storage.cache.name] = sources_tree
        result["last_update"] = self._manager.last_update
        # self._logger.debug("get_all() result: %s", dumps(result, indent=4))
        return wrap_dict_data(result)

    @auto_dbus  # type: ignore
    @last_error
    def get_from_path(self, path: str, maxdepth: int) -> Dict[str, Any]:
        """Return a dict with info about one specific file or folder.

        sample:
        {
            "files": {
                "path": "/var/sl1fw/projects/examples/Petrin_Tower_10H_50um_Prusa_Orange.sl1",
                "type": "file",
                "size": 21008282
            },
            "last_update": 1599112560
        }

        Args:
            path (str): path to the folder or file
            recursive (bool): When is a folder return recursively, the file will ignore it, but it has to be sent
            because of the API dbus architecture.

        Returns:
            Dict[str, Any]
        """
        self._logger.debug("get_from_path('%s', %d)", path, maxdepth)
        pth = Path(path)
        storage = self._manager.path_to_storage(pth)
        result = self._manager.get_files(storage, maxdepth, pth)
        # self._logger.debug("get_from_path() result: %s", dumps(result, indent=4))
        return wrap_dict_data(
            {
                "files": result,
                "last_update": self._manager.last_update,
            }
        )

    @auto_dbus  # type: ignore
    @last_error
    def get_metadata(self, path: str, thumbnail: bool) -> Dict[str, Any]:
        """Extract all metadata from a project file and, optionally, the preview images.
           The user will be responsible for removing the temporary folder afterward.

        sample:
        {
            "files": {
                "path": "/var/sl1fw/projects/examples/Calibration objects/Resin_Calibration_Object_0.050.sl1",
                "type": "file",
                "size": 4304037,
                "metadata": {
                    "action": "print",
                    "jobDir": "Resin calibration object",
                    "expTime": 5,
                    "expTimeFirst": 35,
                    "fileCreationTimestamp": "2019-12-11 at 09:25:00 UTC",
                    "layerHeight": 0.05,
                    "materialName": "Prusa Orange Tough 0.05",
                    "numFade": 10,
                    "numFast": 502,
                    "numSlow": 0,
                    "printProfile": "0.05 Normal",
                    "printTime": 5257.272727,
                    "printerModel": "SL1",
                    "printerProfile": "Original Prusa SL1",
                    "printerVariant": "default",
                    "prusaSlicerVersion": "PrusaSlicer-2.1.0+win64-201909160915",
                    "usedMaterial": 2.135513,
                    "calibrateRegions": 8,
                    "calibrateTime": 1,
                    "calibratePadThickness": 0.5,
                    "calibrateTextThickness": 0.5
                },
                "preview": {
                    "dir": "/tmp/tmpy36n51rt",
                    "images": [
                        "/tmp/tmpy36n51rt/thumbnail/thumbnail400x400.png",
                        "/tmp/tmpy36n51rt/thumbnail/thumbnail800x480.png"
                    ]
                }
            },
            "last_update": 1599112560
        }

        Args:
            path (str): path to the folder or file
            preview (bool): create a temporary folder and unzip all preview files over there

        Returns:
            Dict[str, Any]
        """
        self._logger.debug("get_metadata('%s', %d)", path, thumbnail)
        result = self._manager.item_details(Path(path), with_metadata=True)
        # self._logger.debug("get_metadata() result: '%s'", dumps(result, indent=4))
        return wrap_dict_data(
            {
                "files": result,
                "last_update": self._manager.last_update,
            }
        )

    @auto_dbus
    @last_error
    def remove(self, path: str) -> None:
        """
        remove project
        """
        try:
            self._manager.remove(Path(path))
        except FileNotFoundError as error:
            raise errors.FileNotFound(f"File not found: {path}") from error
        except PermissionError as exc:
            raise errors.ProjectErrorCantRemove() from exc

    @auto_dbus
    @last_error
    def remove_dir(self, path: str, recursive: bool) -> None:
        """
        remove project
        """
        try:
            self._manager.remove(Path(path), recursive)
        except FileNotFoundError as error:
            raise errors.FileNotFound(f"File not found: {path}") from error
        except PermissionError as exc:
            raise errors.ProjectErrorCantRemove() from exc

    @auto_dbus
    def move(self, src: str, dst: str) -> None:
        """
        move project
        """
        try:
            self._manager.move(Path(src), Path(dst))
        except (FileNotFoundError, IsADirectoryError) as error:
            raise errors.FileNotFound(f"File not found: {error.filename}") from error

    @auto_dbus
    def create_folder(self, dst: str) -> None:
        """
        create folder
        """
        self._manager.create_folder(Path(dst))

    @auto_dbus
    @last_error
    def current_project_set(self, path: str) -> None:
        """Point PROJECT_SYMLINK to path
            The file at this path won't be possible to remove without calling clean_protection first.

        Args:
            path (str): Path of the project to print.
        """
        self._manager.current_project = Path(path) if path else None

    @auto_dbus
    @last_error
    def current_project_get(self) -> str:
        """Path to current printing project"""
        return str(self._manager.current_project)

    @auto_dbus
    @last_error
    def current_project_clean(self) -> None:
        """Clean to current printing project"""
        self._manager.current_project = None

    @auto_dbus  # type: ignore
    @property
    def disk_usage(self) -> Dict[str, Any]:
        """Return disk usage statistics about the given path as a dictionary with the attributes
        total, used and free, which are the amount of total, used and free space, in bytes.
        """
        result = {}
        for storage in self._manager.locals:
            if not storage.cache.root.exists:
                continue
            result[storage.cache.name] = disk_usage(storage.cache.root)._asdict()
        return wrap_dict_data(result)

    @auto_dbus  # type: ignore
    @property
    def extensions(self) -> List[str]:
        return PrinterModel.get_model().extensions

    @auto_dbus  # type: ignore
    @property
    def extensions_deprecated(self) -> List[str]:
        return PrinterModel.get_model().extensions_deprecated

    @auto_dbus  # type: ignore
    @property
    def has_deprecated(self) -> Dict[str, bool]:
        result = {}
        for storage in self._manager.locals:
            result[storage.cache.name] = self._manager.has_deprecated(storage)
        return wrap_dict_data(result)

    @auto_dbus
    @last_error
    def remove_all_deprecated_files(self) -> None:
        printer_model = PrinterModel.get_model()
        for storage in self._manager.locals:
            for deprecated in self._manager.fsm.list_files(storage.cache.root, printer_model.extensions_deprecated):
                self._manager.fsm.remove_path(deprecated)

    @auto_dbus
    @last_error
    def move_deprecated_files(self) -> None:
        for storage in self._manager.locals:
            self._manager.archive_deprecated(storage)
        self.PropertiesChanged(
            self.__INTERFACE__,
            {"len_download_deprecated_files": self.len_download_deprecated_files},
            [],
        )

    @auto_dbus
    @last_error
    def remove_downloaded_deprecated_files(self) -> None:
        self._manager.fsm.remove_path(defines.OLD_PROJECTS_DOWNLOAD, recursive=True)
        self.PropertiesChanged(
            self.__INTERFACE__,
            {"len_download_deprecated_files": self.len_download_deprecated_files},
            [],
        )

    @auto_dbus  # type: ignore
    @property
    def len_download_deprecated_files(self) -> int:
        try:
            return len(list(defines.OLD_PROJECTS_DOWNLOAD.iterdir()))
        except FileNotFoundError:
            return 0
