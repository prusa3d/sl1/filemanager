# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2022 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from logging import getLogger

import shutil
import sys
import zipfile
from typing import Callable, List, Tuple, Optional
import time
import tempfile

from . import defines


class Filesystem:
    """Class for accessing filesystems"""

    def __init__(self):
        self._logger = getLogger(__name__)

    def normalize_path(self, path: Optional[Path], make_absolute: bool = True) -> Path:
        """
        Convert path to filesystem encoding and resolve possible symlink.

        Args:
            path (Path): path to check
        Returns:
            Path: resolved absolute path
        """
        if not isinstance(path, Path):
            raise TypeError("path is not instance of Path")
        try:
            path.name.encode(sys.getfilesystemencoding())
        except UnicodeEncodeError as error:
            self._logger.exception("Can't convert '%s' to %s: %s", str(path), sys.getfilesystemencoding(), str(error))
            raise TypeError() from error
        # path.resolve() resolves symlinks (we do not want that) AND eliminate ".." (we MAY want that)
        return path.absolute() if make_absolute else path

    def ch_mode_owner(self, path: Path, group: str, mode: int):
        """
        Change group and mode of the file or folder.

        Args:
            path (Path): filesystem node
            group (str): name of the operating system user group
            mode (int): operating system access mode
        """

        path = self.normalize_path(path)
        shutil.chown(path, group=group)
        path.chmod(mode)

        if Path.is_dir(path):
            for name in Path.iterdir(path):
                self.ch_mode_owner(path / name, group=group, mode=mode)

    def is_mount(self, path: Path) -> bool:
        """
        Check if given path is on mounted node. root and /tmp folders are excluded

        Args:
            path (Path): filesystem node to check for mounted node
        Returns:
            (bool): True when path is on mounted filesystem node, False otherwise
        """
        if self.is_parent(path, Path(tempfile.gettempdir())):
            return False
        if len(path.parents) > 0 and path.is_mount():
            return True
        subfolders_count = len(path.parents) - 1
        return any(path.parents[i].is_mount() for i in range(0, subfolders_count))

    def remove_path(self, path: Path, recursive: bool = False) -> None:
        """
        Remove path

        Args:
            path (Path): filesystem node to be removed
            recursive (bool): True in case of remove all sub nodes
        """
        path = self.normalize_path(path)
        if not path.exists():
            return
        if Path.is_dir(path):
            if recursive:
                shutil.rmtree(path)
            else:
                path.rmdir()
        else:
            path.unlink()

    def list_files(self, path: Path, patterns: List[str], depth: int = defines.MAX_DEPTH) -> List[Path]:
        """
        List files from the given filesystem node to specific depth with partucular patterns

        Args:
            path (Path): starting filesystem node
            patterns (List[str]): list of patterns of accepted files
            depth (int): lelvel of nested
        Returns:
            List[Path]: List of files match the given conditions
        """
        self._logger.debug("list_files() path:'%s' patterns:%s depth:%d", str(path), str(patterns), depth)
        listed_files: List[Path] = []
        try:
            path = self.normalize_path(path)
            for pattern in patterns:
                listed_files.extend(
                    filter(
                        lambda file: len(file.parts) <= depth and not self.is_hidden(file) and file.exists(),
                        path.rglob(pattern),
                    )
                )
        except TypeError:
            self._logger.warning("invalid path '%s'", path)
        return listed_files

    def is_hidden(self, path: Path) -> bool:
        """
        Check if given filesystem node is hidden

        Args:
            path (Path): filesystem node
        Returns:
            bool: True in case of hidden file or parent directory, False otherwise
        """

        try:
            return any(element.startswith(".") for element in self.normalize_path(path).resolve().parts)
        except TypeError as e:
            self._logger.warning("invalid path '%s'. Error: '%s'", path, e)
            return True

    @staticmethod
    def is_parent(path: Path, parent: Path) -> bool:
        """
        Check if given filesystem node is stored beneth the parent node.

        Args:
            path (Path): filesystem node to check
            paren (Path): parent filesystem node
        Returns:
            bool: True in case that path relative to parent isn't empty set, False otherwise
        """

        try:
            path.relative_to(parent)
            return True
        except (ValueError, TypeError):
            return False

    def is_valid_path(self, path: Optional[Path], check: Callable[[Path], bool]) -> bool:
        """
        Check if given path exists, is not hidden and match given condition

        Args:
            path (Path): filesystem node
            check (Callable[[Path], bool]): conditional predicate
        Returns:
            bool: True when given filesystem node match all requirements, False otherwise
        """

        if path is None:
            return False

        if self.is_hidden(path):
            return False

        if path.is_file():
            return check(path)

        return any(check(file) for file in path.rglob("*"))

    def is_valid_project(self, path: Path, metadata_file: str = defines.PROJECT_CONFIG_FILE) -> bool:
        """
        Check if given project is valid zip file contains necessary configuration

        Args:
            path (Path): filesystem file node with 3D project
            metadata_file (str): configuration file required by pronting process
                default value is defines.PROJECT_CONFIG_FILE
        Returns:
            bool: True when all requirements are matched, False otherwise
        """
        try:
            path = self.normalize_path(path)
            if not zipfile.is_zipfile(str(path)):
                return False
            return any(
                filter(
                    lambda filename: filename == metadata_file,
                    zipfile.ZipFile(str(path)).namelist(),
                )
            )
        except TypeError:
            self._logger.warning("invalid path '%s'", path)
            return False

    @staticmethod
    def extract_project_info(project: Path, destination: Path, metadata_file: str, with_thumbnails: bool = True):
        """
        Extract from project zip file essetial configuration and thumbnails images to given destination folder

        Args:
            project (Path): project filesystem node
            destination (Path): folder to where the extracted items will be stored
            metadata_file (str): name of the essetial configuration file
            with_thumnbails (bool): extract also thumbnails files
        """
        with zipfile.ZipFile(str(project)) as zip_file:
            zip_file.extract(metadata_file, path=str(destination))
            if with_thumbnails:
                thumbnails = [filename for filename in zip_file.namelist() if filename.startswith("thumbnail/")]
                for thumbnail in thumbnails:
                    zip_file.extract(thumbnail, path=str(destination))

    def move(self, src: Path, dest: Path):
        """
        Move given file to destination

        Args:
            src (Path): filesystem file node
            dest (Path): destination folder
        """
        src = self.normalize_path(src)
        dest = self.normalize_path(dest)
        if not src.is_file():
            raise TypeError("Not a file")
        shutil.copyfile(str(src), str(dest))
        self.remove_path(src, recursive=True)

    def create_folder(self, dest: Path):
        """
        Creates a folder at destination

        Args:
            dest (Path): destination folder
        """
        dest = self.normalize_path(path=dest)
        if dest.exists():
            return
        dest.mkdir(parents=True, exist_ok=True)

    @staticmethod
    def wait_until(condition: Callable[[], bool], interval=0.1, timeout=1) -> bool:
        """
        Active spin lock wait

        Args:
            condition (Callable[[], bool]): wait predicate
            interval (float): timespan between checks
            timeout (int): number of seconds to wait and check
        Returns:
            bool: True when condition passed before timeout, False otherwise
        """
        start = time.time()
        while not condition():
            if time.time() - start > timeout:
                return False
            time.sleep(interval)
        return True

    @staticmethod
    def available_storage(path: Path, reserved: int = 0) -> Tuple[int, int, int]:
        total, used, free = shutil.disk_usage(str(path))
        available = free - reserved
        return (total, used, available) if available > 0 else (total, used, 0)

    @staticmethod
    def get_mtime(path: Path) -> float:
        return max(path.stat().st_mtime, path.stat().st_ctime)
