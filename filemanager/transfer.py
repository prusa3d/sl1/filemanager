from time import time


class Transfer:
    """
    Class to represent a transfer of a file in progress
    """

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-many-arguments
    # pylint: disable=too-few-public-methods
    _transfer_id: int = 1

    def __init__(self, storage: str, path: str, size: int, transfer_type: str, to_print: bool, start_cmd_id: int):
        self.id: int = Transfer._transfer_id
        Transfer._transfer_id += 1

        self.storage: str = storage
        self.path: str = path
        self.size_total: int = size
        self.size_transferred: int = 0
        self.speed: int = 0
        self.type: str = transfer_type
        self.to_print: bool = to_print
        self.start_cmd_id: int = start_cmd_id
        self.progress: int = 0
        self.time_remaining: int = 0
        self.time_start: int = int(time())
        self.time_elapsed: int = 0

    def update_progress(self, chunk_size: int) -> None:
        self.time_elapsed = int(time() - self.time_start)
        if self.time_elapsed != 0:
            self.size_transferred += chunk_size
            self.progress = int(self.size_transferred / (self.size_total / 100))
            self.speed = int(self.size_transferred / self.time_elapsed)
            if self.speed != 0:
                self.time_remaining = int((self.size_total - self.size_transferred) / self.speed)
