# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2022 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from logging import getLogger
from threading import Lock
from pathlib import Path
from typing import List, Dict, Any, Optional
from hashlib import md5
from dataclasses import dataclass, field, asdict
import json
from time import time
from watchdog.events import PatternMatchingEventHandler, FileSystemEvent, FileSystemMovedEvent
from PySignal import Signal

from . import defines
from .filesystem import Filesystem


@dataclass
class CachedItem:
    """
    Base for cached items
    """

    path: str
    origin: str
    mtime: float

    def __str__(self):
        return str(self.asdict())

    def asdict(self):
        return asdict(self, dict_factory=self._filter)

    @staticmethod
    def _filter(data):
        return dict(x for x in data if x[1] is not None and x[1] != {})


@dataclass
class CachedFolder(CachedItem):
    """
    Cache item - folder
    """

    children: List[CachedItem] = field(default_factory=list)
    type: str = "folder"
    exists: bool = True


@dataclass
class CachedFile(CachedItem):
    """
    Cache item - file
    """

    size: int
    metadata: Optional[Dict[str, Dict[str, str]]] = None
    type: str = "file"
    exists: bool = True


class Cache(PatternMatchingEventHandler):
    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-many-arguments
    """
    Cache representation
    """

    def __init__(self, name: str, root: Path, extensions: List[str], filesystem: Filesystem, updated: Signal):
        """
        Args:
            name (str): cache name (file origin)
            root (Path): root of the cache
            extensions (List[str]): tuple with the files extensions supported
            filesystem (Filesystem): class to access filesystem
            updated (Signal): signal to emit when cache is updated
        """
        self._logger = getLogger(__name__)
        self.name = name
        self.root = filesystem.normalize_path(root)
        self.pattern_list = [f"*{ext}" if ext.startswith(".") else f"*.{ext}" for ext in extensions]
        self.fsm = filesystem
        self.newest_file: Optional[CachedFile] = None
        self.last_update = time()
        self._cache_dir = defines.CACHE_DIR / name
        self._updated = updated
        self._items: Dict[str, CachedItem] = {}
        self._update_lock = Lock()
        super().__init__(patterns=self.pattern_list, ignore_directories=True)
        self.add(self.root, notify=False)

    def __str__(self):
        """
        Cache as string
        """
        return (
            f"Cache name={self.name} root={self.root} pattern_list={self.pattern_list} cache_dir={self._cache_dir} "
            f"item_count={len(self._items)}"
        )

    @staticmethod
    def _get_hash(path: Path) -> str:
        return md5(str(path).encode("utf-8"), usedforsecurity=False).hexdigest()  # type: ignore[call-arg]

    def get(self, item: Optional[Path], with_metadata: bool = False) -> CachedItem:
        """
        Retrieve file or directory from cache.

        Args:
            item (Path): requested item path
            with_metadata (bool): also return project metadata
        Returns:
            Dict[str, Any]: item data
        """
        try:
            path = self.fsm.normalize_path(item)
            key = self._get_hash(path)
            with self._update_lock:
                data = self._items.get(key, None)
                if data is None:
                    self._logger.warning("'%s' is not cached", item)
                    return CachedFile(str(item), self.name, 0, 0, exists=False)
                if with_metadata and isinstance(data, CachedFile) and data.metadata is None:
                    if self.fsm.is_valid_project(path):
                        self._logger.debug("getting metadata of '%s'", path)
                        cache_folder = self._cache_dir / key
                        cache_folder.mkdir(parents=True, exist_ok=True)
                        self.fsm.ch_mode_owner(cache_folder, defines.PROJECT_GROUP, defines.PROJECT_DIR_CACHE)
                        self.fsm.extract_project_info(path, cache_folder, defines.PROJECT_CONFIG_FILE)
                        data.metadata = {
                            "config": self._get_config(cache_folder),
                            "thumbnail": self._get_thumbnails(cache_folder),
                        }
                    else:
                        self._logger.warning("Not valid project: '%s'", path)
                        data.metadata = {}
                return data
        except TypeError:
            self._logger.warning("'%s' has invalid name to get", item)
            return CachedFile(str(item), self.name, 0, 0, exists=False)

    def add(self, item: Path, notify: bool = True, sort: bool = True) -> Optional[CachedItem]:
        """
        Create the cache record from item

        Args:
            item (Path): new item path
            notify (bool): emit updated signal
            sort (bool): sort tree by mtime
        """
        self._logger.debug("add item '%s'", item)
        try:
            path = self.fsm.normalize_path(item)
            key = self._get_hash(path)
            if key in self._items:
                self._logger.warning("'%s' is already cached", item)
                return None
            # check the tree
            parent_path = path.parent
            parent_key = self._get_hash(parent_path)
            parent_data = self._items.get(parent_key, None)
            if not isinstance(parent_data, CachedFolder) and path != self.root:
                parent_data = self.add(parent_path, notify=False, sort=sort)
            with self._update_lock:
                mtime = self.fsm.get_mtime(path)
                data: Optional[CachedItem] = None
                if path.is_dir():
                    data = CachedFolder(str(path), self.name, mtime)
                else:
                    data = CachedFile(str(path), self.name, mtime, path.stat().st_size)
                    if not self.newest_file or data.mtime > self.newest_file.mtime:
                        self.newest_file = data
                self._items[key] = data
                # add item to the tree
                if isinstance(parent_data, CachedFolder):
                    parent_data.children.append(data)
                    if sort:
                        parent_data.children.sort(key=lambda item: item.mtime, reverse=True)
                elif path != self.root:
                    self._logger.warning("add - no parent for '%s'", item)
            self.last_update = time()
            if notify:
                self._updated.emit(self.last_update)
            return data
        except TypeError:
            self._logger.warning("'%s' has invalid name to contruct", item)
            return None

    def sort_tree(self):
        self._logger.debug("sort_tree '%s'", self.root)
        key = self._get_hash(self.root)
        with self._update_lock:
            data = self._items.get(key, None)
            if isinstance(data, CachedFolder):
                self._sort_tree(data)
        self.last_update = time()
        self._updated.emit(self.last_update)

    def _sort_tree(self, cache_data: CachedFolder):
        cache_data.children.sort(key=lambda item: item.mtime, reverse=True)
        for child in cache_data.children:
            if isinstance(child, CachedFolder):
                self._sort_tree(child)

    def remove(self, item: Path, notify: bool = True):
        """
        Destroy the cache record with file details for given item key

        Args:
            item (Path): requested item path
            notify (bool): emit updated signal
        """
        self._logger.debug("delete item '%s'", item)
        try:
            path = self.fsm.normalize_path(item)
            key = self._get_hash(path)
            with self._update_lock:
                if key not in self._items:
                    self._logger.warning("'%s' is not cached", item)
                    return
                # remove itemfrom the tree
                parent_path = path.parent
                parent_key = self._get_hash(parent_path)
                parent_data = self._items.get(parent_key, None)
                if isinstance(parent_data, CachedFolder):
                    parent_data.children.remove(self._items[key])
                elif len(self._items) != 1:
                    self._logger.warning("remove - no parent for '%s'", item)
                del self._items[key]
                # remove files cache from filesystem
                cache_folder = self._cache_dir / key
                self.fsm.remove_path(cache_folder, recursive=True)
            if isinstance(parent_data, CachedFolder) and not parent_data.children and parent_path != self.root:
                self._logger.debug("empty parent '%s'", parent_path)
                self.remove(parent_path, notify=False)
            self.last_update = time()
            if notify:
                self._updated.emit(self.last_update)
        except TypeError:
            self._logger.warning("'%s' has invalid name to remove", item)

    def discard_all(self) -> None:
        """
        Destroy entire cache. Used for removable storages
        """
        with self._update_lock:
            self.fsm.remove_path(self._cache_dir, recursive=True)
            self._items = {}
        self.last_update = time()
        self._updated.emit(self.last_update)

    @staticmethod
    def _get_thumbnails(cache_folder: Path) -> Dict[str, Any]:
        """
        Get project thumbnails in case the cache item key exists in format
            {
                "files": [
                    thumbnail1,
                    thumbnail2,
                ],
                "dir": "/thumnbnails/folder
            }

        Returns:
            Dict[str, Any]: information about project thumbnails
        """
        files = [str(cache_folder / file) for file in cache_folder.glob("thumbnail/*")]
        return {"files": files, "dir": str(cache_folder)}

    @staticmethod
    def _get_config(cache_folder: Path) -> Dict[str, Any]:
        """
        Get project configuration in case the cache item key exists in JSON format.
        Configuration stored in defines.PROJECT_CONFIG_FILE file inside the project

        Returns:
            Dict[str, Any]: project configuration
        """
        config: Dict[str, Any] = {}
        with open(cache_folder / defines.PROJECT_CONFIG_FILE, encoding="utf-8") as f_in:
            for line in filter(None, (line.rstrip() for line in f_in)):
                try:
                    (key, value) = line.split(" = ")
                    config[key] = json.loads(value)
                except json.decoder.JSONDecodeError:
                    config[key] = value
        return config

    # EventHandler stuff
    def on_moved(self, event: FileSystemMovedEvent):
        """Called when a file or a directory is moved or renamed."""
        self._logger.debug("Event moved:'%s'->'%s' directory:%s", event.src_path, event.dest_path, event.is_directory)
        src_path = Path(event.src_path)
        if not self.fsm.is_hidden(src_path):
            self.remove(src_path)
        dest_path = Path(event.dest_path)
        if not self.fsm.is_hidden(dest_path):
            self.add(dest_path)

    def on_created(self, event: FileSystemEvent):
        """Called when a file or directory is created."""
        self._logger.debug("Event %s:'%s' directory:%s", event.event_type, event.src_path, event.is_directory)
        src_path = Path(event.src_path)
        if not self.fsm.is_hidden(src_path):
            self.add(src_path)

    def on_deleted(self, event: FileSystemEvent):
        """Called when a file or directory is deleted."""
        self._logger.debug("Event %s:'%s' directory:%s", event.event_type, event.src_path, event.is_directory)
        src_path = Path(event.src_path)
        if not self.fsm.is_hidden(src_path):
            self.remove(src_path)
