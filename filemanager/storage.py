# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2022 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from logging import getLogger
from pathlib import Path
from typing import List
import watchdog.observers.polling as WatcherPoll

from . import defines
from .cache import Cache


class Storage:
    """
    Storage abstraction

    Storage contain source tree, linked source monitor if it's required and projects cache represetation.
    """

    def __init__(self, cache: Cache, removable: bool = False):
        """
        Args:
            cache (Cache): cache in case of cached source
            removable (bool): indicates that storage is removable
                            Default: False
        """
        self._logger = getLogger(__name__)
        self._removable = removable
        self._cache = cache
        self._observer = WatcherPoll.PollingObserver()

        if not removable:
            self._logger.debug("Starting fs monitor for '%s'", str(cache.root))
            self._observer.schedule(self._cache, str(cache.root), True)
            self._observer.start()

    def __del__(self):
        """
        Stops monitoring and cleans cache in case of removable storage
        """
        if self.is_monitored:
            self._logger.debug("stopping fs monitor for '%s'", str(self.cache.root))
            self._observer.stop()
            self._observer.join()
        if self.is_removable:
            self._cache.discard_all()

    def __str__(self):
        """
        Storage as string
        """
        return f"Storage cache={self._cache} removable={self._removable}"

    @property
    def is_removable(self) -> bool:
        """
        True when storage is removable (USB), False otherwise
        """
        return self._removable

    @property
    def is_monitored(self) -> bool:
        """
        True when source is monitored for changes, False otherwise
        """
        return self._observer.is_alive()

    @property
    def cache(self) -> Cache:
        """
        Cache representation
        """
        return self._cache

    def list_files(self, depth: int = defines.MAX_DEPTH) -> List[Path]:
        """
        List of acceptable projects stored in the source tree, except those
        stored under the hidden folder or hidden themselfs

        Args:
            depth (int): the level of nesting folders
                        default value = maxsize means: no nesting restrictions
        Returns:
            List[Path]: list of filepaths
        """
        return self._cache.fsm.list_files(self.cache.root, self.cache.pattern_list, depth)

    def fill_cache(self):
        for item in self.list_files():
            self._cache.add(item, notify=False, sort=False)
        self._cache.sort_tree()
