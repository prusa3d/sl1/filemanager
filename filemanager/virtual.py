import logging
from pathlib import Path
from unittest.mock import patch, Mock

from filemanager.__main__ import main

logging.basicConfig(format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=logging.DEBUG)

storage = Path(__file__).parent.parent / "storage"
with (
    patch("filemanager.defines.PROJECT_ROOT_PATH", storage / "projects"),
    patch("filemanager.defines.OLD_PROJECTS_DOWNLOAD", storage / "old-projects"),
    patch("filemanager.defines.PROJECT_SYMLINK", storage / "previous-prints"),
    patch("filemanager.__main__.configure_log", Mock),  # log into current terminal instead of journalctl
):
    main()
