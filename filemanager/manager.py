# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from logging import getLogger
from pathlib import Path
from subprocess import Popen, PIPE  # nosec
from typing import Dict, Any, List, Optional, Union
from copy import copy
from time import time
import pyudev
from PySignal import Signal

from . import defines, errors, usb

from .filesystem import Filesystem
from .model import PrinterModel
from .storage import Storage
from .cache import Cache, CachedItem, CachedFolder


def execute(cmd: List[str]) -> bool:
    """
    Execute command and return exit code and output

    Args:
        cmd (List[str]): command to execute
    Returns:
        Tuple[int, str]: exit code and output
    """
    getLogger(__name__).debug("Executing %s", cmd)
    with Popen(cmd, stdout=PIPE, stderr=PIPE) as process:  # nosec
        output, error = process.communicate()
        if process.returncode != 0:
            getLogger(__name__).error(
                "Error executing %s: %s, %s", str(cmd), output.decode("utf-8"), error.decode("utf-8")
            )
        return process.returncode == 0


# pylint: disable = too-many-public-methods
# pylint: disable = too-many-instance-attributes
class Manager:
    """Storage container"""

    # keep at least 110 MB of free space when copying project
    # to internal storage or extracting examples
    INTERNAL_RESERVED_SPACE = 110 * 1024 * 1024

    def __init__(self, model: PrinterModel):
        """
        Args:
            model (PrinterModel): model of the printer
        """
        self._logger = getLogger(__name__)
        self.updated = Signal()
        self._current_media_path = ""
        self.current_media_path_changed = Signal()
        self.one_click_print_file = Signal()
        self.media_inserted = Signal()
        self.media_ejected = Signal()
        self.busy_changed = Signal()
        self._media_present = False
        self.media_present_changed = Signal()
        self._media_mounted = False
        self.media_mounted_changed = Signal()
        self.fsm = Filesystem()
        self._model = model
        # pylint: disable=unnecessary-lambda
        self._usb_observer = usb.monitor_factory(lambda device: self.usb_event_handler(device))
        self._usb_write_monitor = usb.WriteMonitor()
        self._usb_write_monitor.busy_changed.connect(lambda busy: self.busy_changed.emit(busy))
        if model is PrinterModel.NONE:
            raise RuntimeError("Unknown printer model")

        local_cache = Cache("local", defines.PROJECT_ROOT_PATH, self._model.extensions, self.fsm, self.updated)
        local_storage = Storage(local_cache)
        local_storage.fill_cache()

        previous_prints_cache = Cache(
            "previous-prints", defines.PROJECT_SYMLINK, self._model.extensions, self.fsm, self.updated
        )
        previous_prints_storage = Storage(previous_prints_cache)
        previous_prints_storage.fill_cache()

        self._storages = [local_storage, previous_prints_storage]

        if (dev := usb.inserted_device()) is not None:
            # Having it locally should avoid it being accidentally instantiated anywhere else
            class FakeDevice:  # pylint: disable=too-few-public-methods
                """
                Single-use fake device for USB event handler to handle the initial USB insertion event.
                Should be a drop-in replacement of pyudev.Device.
                """

                def __init__(self, device_node: str, action: str, fs_type: str, fs_label: str):
                    self.device_node = device_node
                    self.action = action
                    self.fs_type = fs_type
                    self.fs_label = fs_label

                def get(self, key: str) -> str:
                    if key == "ID_FS_TYPE":
                        return self.fs_type
                    if key == "ID_FS_LABEL":
                        return self.fs_label
                    return ""

            _dev = FakeDevice(dev.device_node, "add", dev.get("ID_FS_TYPE"), dev.get("ID_FS_LABEL"))
            self.usb_event_handler(_dev)
        self.start_monitor()

    @property
    def busy(self):
        return self._usb_write_monitor.busy

    @property
    def current_media_path(self):
        return self._current_media_path

    @current_media_path.setter
    def current_media_path(self, value: str):
        if value != self._current_media_path:
            self._current_media_path = value
            self.current_media_path_changed.emit(value)

    @property
    def media_present(self):
        return self._media_present

    @media_present.setter
    def media_present(self, value):
        if value != self._media_present:
            self._media_present = value
            self.media_present_changed.emit(value)

    @property
    def media_mounted(self):
        return self._media_mounted

    @media_mounted.setter
    def media_mounted(self, value):
        if value != self._media_mounted:
            self._media_mounted = value
            self.media_mounted_changed.emit(value)

    def extensions(self):
        return self._model.extensions

    def start_monitor(self):
        """
        Start observing USB slot
        """
        if not self._usb_observer.is_alive():
            self._usb_observer.start()

    def stop_monitor(self):
        """
        Stop observing USB slot
        """
        if self._usb_observer.is_alive():
            self._usb_observer.stop()

    @property
    def removable_presented(self) -> str:
        """
        Check if any USB mass storage device is plugged and returns it's name

        Returns:
            str: name of the plugged USB device (sda) or empty string otherwise
        """
        return usb.inserted().stem

    def __del__(self):
        """
        Stop monitoring when Manager instance is destroyed
        """
        self.stop_monitor()

    def insert_usb(self, path: Path) -> Storage:
        """
        Construct all storages assigned with USB mass storage device. In this case
        project storage with cache and monitoring and update storage without cache
        nor monitoring

        Args:
            path (Path): filesystem node where the USB mass storage device is mounted
        Returns:
            Storage: Constructed project storage
        """
        usb_cache = Cache("usb", path, self._model.extensions, self.fsm, self.updated)
        usb_storage = Storage(usb_cache, removable=True)
        usb_storage.fill_cache()
        self._storages.append(usb_storage)

        raucb_cache = Cache("raucb", path, [".raucb"], self.fsm, self.updated)
        raucb_storage = Storage(raucb_cache, removable=True)
        raucb_storage.fill_cache()
        self._storages.append(raucb_storage)

        return usb_storage

    def remove_usb(self) -> None:
        """
        Remove all removable storages
        """
        self._storages = self.locals

    def is_mounted(self, path: Path) -> bool:  # pylint: disable=no-self-use
        return path.exists() and path.is_mount()

    def umount_usb(self, path: Path) -> bool:
        """
        Unmount USB mass storage device

        Args:
            path (Path): filesystem node where the USB mass storage device is mounted
        """
        self._logger.info("Unmounting USB")
        assert self.is_mounted(path)  # nosec

        if self.busy:
            self._logger.warning("Can't unmount USB, printer is busy")
            return False

        r = execute(["systemctl", "stop", f"run-media-system-{path.parts[-1]}.automount"])
        if r:
            assert not self.is_mounted(path)  # nosec
            self._logger.info("USB unmounted")
            self.remove_usb()
            self.current_media_path = ""
            self.media_mounted = False
        return r

    def mount_usb(self) -> bool:
        """
        Mount USB mass storage device

        Args:
            path (Path): filesystem node where the USB mass storage device is mounted
        """

        def usb_drive_filter(path: Path) -> bool:
            """Check, that a given path is a block device partition"""
            return "sd" in path.parts[-1] and path.parts[-1][-1].isdigit() and path.is_block_device()

        try:
            self._logger.warning("Mounting USB")

            if self.busy:
                self._logger.warning("Can't mount USB, printer is busy")
                return False

            # First drive should be enough, we don't expect more than one anyway
            drive = next(filter(usb_drive_filter, Path("/dev").iterdir()))
            if self.is_mounted(defines.USB_MOUNT_POINT / drive.stem):
                self._logger.warning("USB already mounted")
                return False
            return execute(["usbmount", f"--owner={defines.PROJECT_GROUP}", str(drive)])
        except:  # pylint: disable=bare-except
            self._logger.error("Failed to mount USB")
            return False

    def usb_event_handler(self, device: Union[pyudev.Device, Any]):
        """
        udev USB monitor handler handeling add and remove USB mass storage

        Args:
            device (pyudev.Device): USB device
        """

        if not device.get("ID_FS_TYPE"):
            return

        mount_path = defines.USB_MOUNT_POINT / Path(device.device_node).stem
        self._logger.debug("%s %s at %s", device.action, device.get("ID_FS_LABEL"), mount_path)
        if device.action == "add":
            self._usb_write_monitor.start(Path("/dev") / Path(self.removable_presented[0:-1]))
            self.media_inserted.emit(str(mount_path))
            self.media_present = True

            is_mounted = self.fsm.wait_until(condition=lambda: self.is_mounted(mount_path), timeout=2, interval=0.5)
            if not is_mounted:
                self.media_mounted = False
                self._logger.error("automount of %s timeouted", device.get("ID_FS_LABEL"))
                # If it's not mounted, don't leave the automount service running
                execute(["systemctl", "stop", f"run-media-system-{device.get('ID_FS_LABEL')}.automount"])
            else:
                self.media_mounted = True
                self.current_media_path = str(mount_path)
                self._logger.debug("USB mounted at %s", mount_path)
                try:
                    usb_storage = self.insert_usb(mount_path)
                    newest_file = usb_storage.cache.newest_file
                    if newest_file:
                        self.one_click_print_file.emit(Path(newest_file.path))
                except OSError as e:  # Handle I/O error when USB is removed during loading
                    self._logger.error("Failed to insert USB: %s", e)
                    self.remove_usb()
                self.updated.emit(time())
        elif device.action == "remove":
            self.remove_usb()
            self.current_media_path = ""
            self.media_mounted = False
            self.media_present = False
            self.media_ejected.emit(str(mount_path))
            self._usb_write_monitor.stop()
        elif device.action == "change":
            self._logger.debug(
                "USB change event path: %s, is_mounted: %s, usb.inserted: %s",
                str(mount_path),
                str(self.is_mounted(mount_path)),
                str(self.removable_presented),
            )

    @property
    def storages(self) -> List[Storage]:
        """
        All available storages
        """
        return self._storages

    @property
    def locals(self) -> List[Storage]:
        """
        ALl not removable storages
        """
        return [local for local in self.storages if not local.is_removable]

    @property
    def removable(self) -> List[Storage]:
        """
        All removable storages
        """
        return [removable for removable in self.storages if removable.is_removable]

    def path_to_storage(self, path: Path) -> Storage:
        """
        Search for storage with given filesystem node. In case the storage doesn't exist
        the exception FileNotFoundError is thrown

        Args:
            path (Path): filesystem node
        Returns:
            Storage: storage contains such a filesystem node
        """
        try:
            path = self.fsm.normalize_path(path, make_absolute=False)
            return next(filter(lambda storage: self.fsm.is_parent(path, storage.cache.root), self._storages))
        except StopIteration as exc:
            raise FileNotFoundError() from exc
        except TypeError as exc:
            raise FileNotFoundError() from exc

    def path_has_storage(self, path: Path) -> bool:
        """
        Check if given filesystem node is in any available storage

        Args:
            path (Path): filesystem node
        Returns:
            bool: True when such a storage exists, False otherwise
        """
        try:
            self.path_to_storage(path)
            return True
        except FileNotFoundError:
            return False

    @property
    def current_project(self) -> Path:
        """
        Currently printing project

        Returns:
            str: path to the currently printing project or empty string otherwise
        """
        storage = self.path_to_storage(defines.PROJECT_SYMLINK)
        projects = storage.list_files()
        if len(projects) > 0:
            return projects[0]
        return Path("")

    @current_project.setter
    def current_project(self, path: Optional[Path]) -> None:
        """
        Sets/clean current printing project. When path doesn't exist the exception
        FileNotFound is thrown

        Args:
            path (Path): path to the project which is going to print.
                When path is None or inconvertible to filesystem node,
                the current printing project is cleaned
        """
        try:
            path = self.fsm.normalize_path(path)
            storage = self.path_to_storage(path)
            project = defines.PROJECT_SYMLINK / path.name
            self.clean_current_project()
            if not storage.is_removable:
                self._logger.debug("Internal storage project, creating symlink '%s' -> '%s'", path, project)
                project.symlink_to(path)
            else:
                self._logger.debug("External storage project, copying '%s' -> '%s'", path, project)
                tmp_file = defines.PROJECT_SYMLINK / (path.name + "~")
                _, _, size_available = self.fsm.available_storage(defines.STORAGE_PATH, self.INTERNAL_RESERVED_SPACE)
                self._logger.debug("Internal storage available space: %d MB", (size_available // (2**20)))
                filesize = path.stat().st_size
                self._logger.debug("Project size: %d kB", (filesize // (2**10)))
                if size_available < filesize:
                    raise IOError("Not enough free space, printing directly from USB.")

                self._logger.debug("Copying project to internal storage '%s' -> '%s'", str(path), str(project))
                tmp_file.write_bytes(path.read_bytes())
                self.fsm.move(tmp_file, project)
                self._logger.debug("Done copying project")

                self.fsm.ch_mode_owner(project, defines.PROJECT_GROUP, defines.PROJECT_MODE)
        except TypeError:
            self.clean_current_project()
        except FileNotFoundError as error:
            raise errors.FileNotFound(f"File not found: {path}") from error

    def clean_current_project(self) -> None:
        self._logger.debug("Cleaning previous-prints")
        for file in defines.PROJECT_SYMLINK.glob("*"):
            file.unlink()

    @property
    def last_update(self) -> Dict[str, float]:
        """
        Dictionary of timestamps for all available storages
        """
        return {s.cache.name: s.cache.last_update for s in self._storages}

    def item_details(self, path: Path, with_metadata: bool = False):
        """
        All details of the given path.

        Args:
            path (Path): item path
            with_metadata (bool): Also return project metadata if available
        Returns:
            Dict[str, Any]: All information about given item as dictionary
        """
        storage = self.path_to_storage(path)
        return storage.cache.get(path, with_metadata).asdict()

    def move(self, src: Path, dest: Path):
        """
        Move project from one place to another

        Args:
            src (Path): original project
            dest (Path): new project placement
        """
        try:
            self.fsm.move(src, dest)
            try:
                from_storage = self.path_to_storage(src)
                if from_storage not in self.locals:
                    from_storage.cache.remove(src)
            except FileNotFoundError:
                self._logger.warning("Source storage not found: %s", src)
            to_storage = self.path_to_storage(dest)
            if to_storage in self.locals:
                self.fsm.ch_mode_owner(dest, defines.PROJECT_GROUP, defines.PROJECT_MODE)
            to_storage.cache.remove(dest)
            to_storage.cache.add(dest)
        except TypeError as err:
            raise FileNotFoundError() from err

    def create_folder(self, dest: Path):
        """
        Creates a new folder

        Args:
            dest (Path): new folder placement
        """
        try:
            self.fsm.create_folder(dest=dest)
            to_storage = self.path_to_storage(path=dest)
            if to_storage in self.locals:
                self.fsm.ch_mode_owner(
                    path=dest,
                    group=defines.PROJECT_GROUP,
                    mode=defines.PROJECT_DIR_MODE,
                )
            to_storage.cache.add(dest)
        except Exception as e:
            raise IOError(f"Create folder failed: {dest}") from e

    def remove(self, path: Path, recursive: bool = True):
        """
        Delete file or folder from the filesystem

        Args:
            path (Path): filesystem node to delete
            recursive (bool): delete also all nested items
        """
        storage = self.path_to_storage(path)
        if path == storage.cache.root:
            raise PermissionError()
        self.fsm.remove_path(path, recursive)
        storage.cache.remove(path)

    def has_deprecated(self, storage: Storage) -> bool:
        """
        Check if storage contains deprecated projects

        Args:
            storage (Storage): given storage to check for deprecated
        Returns:
            bool: True when given storage contains deprecated projects, False otherwise
        """
        return any(self.fsm.list_files(storage.cache.root, self._model.extensions_deprecated))

    def archive_deprecated(self, storage: Storage) -> None:
        """
        Archive deprecated projects to dedicated place. In case that dedicated place
        already contains archive with the same name, the method will generate new
        name to prevent override of the existing one.

        Args:
            storage (Storage): given storage to seek for deprecated projects
        """
        dest_folder = defines.OLD_PROJECTS_DOWNLOAD
        dest_folder.mkdir(parents=True, exist_ok=True)
        for deprecated in self.fsm.list_files(storage.cache.root, self._model.extensions_deprecated):
            if deprecated.is_symlink():
                deprecated.unlink()
                continue
            file_name = str(deprecated.relative_to(storage.cache.root)).replace("/", "_")
            destination = dest_folder / file_name
            count = 1
            while destination.exists():
                destination = Path(f"{str(dest_folder / destination.stem)}_copy{count}{deprecated.suffix}")
                count += 1
            self.fsm.move(deprecated, destination)

    def get_files(self, storage: Storage, depth: int, path: Optional[Path] = None) -> Dict[str, Any]:
        """
        Gather all files and directories stored under the given path in the specify depth.

        Args:
            storage (Storage): storage to get from
            depth (int): level of nesting during the walk through directories
            path (Path): starting filesystem node to gather the files, whole Storage when None
        Returns:
            List[CachedItem] cached filesystem hierarchy
        """

        base_item = storage.cache.get(path if path else storage.cache.root)
        if not isinstance(base_item, CachedFolder) or depth < 0:
            # return whole tree without walking it
            return base_item.asdict()
        new_base_item = copy(base_item)
        new_base_item.children = self._walk_path(base_item, depth - 1)
        return new_base_item.asdict()

    def _walk_path(self, base_item: CachedFolder, depth: int) -> List[CachedItem]:
        items: List[CachedItem] = []
        for item in base_item.children:
            if isinstance(item, CachedFolder):
                new_item = copy(item)
                if depth > 0:
                    new_item.children = self._walk_path(item, depth - 1)
                else:
                    new_item.children = []
                items.append(new_item)
            else:
                items.append(item)
        return items
