# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2022 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

__version__ = "0.3.0"
__date__ = "09 Oct 2023"

__copyright__ = "(c) 2022-2023 Prusa 3D"
__author_name__ = "Marek Mosna"
__author_email__ = "marek.mosna@prusa3d.cz"
__author__ = f"{__author_name__} <{__author_email__}>"
__description__ = "Prusa SLA File Manager"

__credits__ = "Marek Mosna"
__url__ = "https://gitlab.com/prusa3d/sl1/filemanager.git"
