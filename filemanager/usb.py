# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2022 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import fcntl
import os
import logging
from typing import Optional
from subprocess import Popen, PIPE  # nosec
from threading import Thread
from pathlib import Path
import selectors
import pyudev
from PySignal import Signal


_common_context = pyudev.Context()


def monitor_factory(callback) -> pyudev.MonitorObserver:
    """
    Construct USB monitoring observer

    Args:
        callback: method invoked whenever change on USB slot occurs
    Returns:
        MonitorObserver: instance of observer
    """
    monitor = pyudev.Monitor.from_netlink(_common_context)
    monitor.filter_by("block")
    return pyudev.MonitorObserver(monitor, callback=callback)


def inserted_device(context: pyudev.Context = _common_context) -> Optional[pyudev.Device]:
    """
    Get device where the USB is currently inserted. In case USB is not mounted,
    None is returned

    Returns:
        pyudev.Device: device where the USB is currently inserted or None
    """
    for device in context.list_devices(subsystem="block", DEVTYPE="partition"):
        if device.get("ID_USB_DRIVER") == "usb-storage":
            return device
    return None


def inserted(context: pyudev.Context = _common_context) -> Path:
    """
    Get device name where the USB is currently inserted. In case USB is not mounted,
    the empty string is returned

    Returns:
        str: name of the mounted device or empty string
    """
    dev = inserted_device(context)
    return Path(dev.device_node) if dev is not None else Path()


class Blktrace:
    """Context manager for the blktrace utility"""

    def __init__(self, device_path: Path):
        self._device_path = device_path
        self._child: Optional[Popen] = None  # type: ignore
        self.cmd = ["blktrace", str(self._device_path), "-a", "queue", "-o", "-"]
        self._logger = logging.getLogger(self.__class__.__name__ + "(" + str(device_path) + ")")
        if not self._device_path.is_block_device():
            self._logger.error("%s is not a block device", str(self._device_path))
            raise ValueError(f"{self._device_path} is not a block device")

    def __enter__(self):
        self._logger.debug("Child started: %s", str(self.cmd))
        self._child = Popen(self.cmd, bufsize=0, stdout=PIPE)  # nosec
        return self._child

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._logger.debug("Child about to terminate")
        self._child.terminate()
        try:
            self._child.wait(timeout=3)
            self._logger.info("Child terminated")
        except TimeoutError:
            self._logger.error("Child did not terminate in time, killing it")
            self._child.kill()


class WriteMonitor:  # pylint: disable=too-many-instance-attributes
    """
    Monitor the writes on the block device.

    Description:
        It uses the blktrace utility to monitor the writes on the block device. The writes are detected by the presence
        of the events in the stdout of the blktrace utility.

        A worker thread is created to set the busy property whenever write event is detected and to emit the
        busy_changed signal.
    Args:
        device_path (Path): path to the block device

    Attributes:
        busy_changed (Signal): signal emitted whenever the busy state changes
        busy (bool): whether the device is currently busy

    Usage:
        monitor = WriteMonitor(Path("/dev/sda"))
        monitor.busy_changed.connect(lambda busy: print("Device is busy" if busy else "Device is idle"))
        monitor.start()
        sleep(100)  # do something
        monitor.stop()

    Note:
        The busy_changed signal is emitted and will be processed by the worker thread, not the main thread.
    """

    def __init__(self, device_path: Path = Path(), start: bool = False, timeout: float = 1.0):
        self._device_path = device_path
        self._keep_running = True
        self._timeout = timeout
        self._busy = False
        self.busy_changed = Signal()
        self._logger = logging.getLogger(self.__class__.__name__ + "(" + str(self._device_path) + ")")
        self._thread: Optional[Thread] = None
        if start:
            self.start()

    def start(self, device_path: Optional[Path] = None):
        """
        Start the worker thread, it will start monitoring the writes on the block device.

        If the worker thread is already running, it will be stopped and started again with the new device path.

        Args:
            device_path (Path): path to the block device
        """
        if self._thread is not None and self._thread.is_alive():
            self.stop()
        self._logger.info("Starting the worker thread for %s", str(device_path))
        if device_path:
            self._device_path = device_path
        self._keep_running = True
        self._thread = Thread(target=self._thread_func, name="WriteMonitorThread")
        self._thread.start()

    def stop(self):
        """Stop the worker thread"""
        self._logger.info("Stopping the worker thread for %s", str(self._device_path))
        assert self._thread is not None  # nosec
        self._keep_running = False
        self._thread.join(2 * self._timeout)
        self.busy = False

    def __del__(self):
        if self._thread and self._thread.is_alive():
            self.stop()

    @property
    def busy(self):
        """There is recent write activity on the block device. Within the timeout period."""
        return self._busy

    @busy.setter
    def busy(self, value: bool):
        if value != self._busy:
            self._busy = value
            self.busy_changed.emit(self._busy)

    def _thread_func(self):
        with Blktrace(self._device_path) as blktrace:
            # Make the stdout non-blocking. Fixme: There must be a better way to do this.
            stdout_fd = blktrace.stdout.fileno()
            flags = fcntl.fcntl(stdout_fd, fcntl.F_GETFL)
            fcntl.fcntl(stdout_fd, fcntl.F_SETFL, flags | os.O_NONBLOCK)
            selector = selectors.DefaultSelector()
            selector.register(blktrace.stdout, selectors.EVENT_READ)
            while self._keep_running:
                events = selector.select(timeout=self._timeout)
                # We are interested only in the events (their existence) itself to indicate that there is write activity
                # on the block device, not in the actual data.
                if events:
                    for event in events:
                        # Read it to remove it from the buffer
                        event[0].fileobj.read()  # type: ignore
                    self.busy = True
                else:
                    # Timeout, no events -> not busy
                    self.busy = False
