# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2022 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from pathlib import Path
from shutil import rmtree

import pytest

from filemanager import defines


@pytest.fixture(autouse=True)
def session_mock(
    monkeypatch,
    cache_filesystem,
    usb_filesystem,
    previous_prints_filesystem,
):
    """Mock the dbus and examples path"""
    monkeypatch.setattr(defines, "PROJECT_ROOT_PATH", Path(os.environ["EXAMPLES_PROJECTS"]))
    monkeypatch.setattr(defines, "PROJECT_SYMLINK", previous_prints_filesystem)
    monkeypatch.setattr(defines, "USB_MOUNT_POINT", usb_filesystem)
    monkeypatch.setattr(defines, "CACHE_DIR", cache_filesystem)
    monkeypatch.setattr(defines, "PRINTER_MODEL_PATH", Path(os.environ["PRINTER_MODEL_PATH"]))


@pytest.fixture(name="root_filesystem")
def fixture_root_filesystem(tmp_path):
    root = tmp_path / "root"
    root.mkdir(parents=True, exist_ok=True)
    yield root
    rmtree(root)


@pytest.fixture(name="cache_filesystem")
def fixture_cache_filesystem(tmp_path):
    root = tmp_path / "cache"
    root.mkdir(parents=True, exist_ok=True)
    yield root
    rmtree(root)


@pytest.fixture(name="usb_filesystem")
def fixture_usb_filesystem(tmp_path):
    root = tmp_path / "usb"
    root.mkdir(parents=True, exist_ok=True)
    yield root
    rmtree(root)


@pytest.fixture(name="previous_prints_filesystem")
def fixture_previous_prints_filesystem(tmp_path):
    root = tmp_path / "previous-prints"
    root.mkdir(parents=True, exist_ok=True)
    yield root
    rmtree(root)
