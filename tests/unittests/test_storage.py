from pathlib import Path
from shutil import rmtree
from unittest.mock import call

from filemanager.storage import Storage
from filemanager.filesystem import Filesystem


# =========================storage initialization==================================================
def test_start_and_stop(mocker):
    mock_cache = mocker.MagicMock()

    storage = Storage(mock_cache, removable=True)

    assert storage.is_removable
    assert not storage.is_monitored
    assert storage.cache == mock_cache
    assert str(storage) == f"Storage cache={mock_cache} removable=True"

    del storage
    assert mock_cache.mock_calls[-1] == call.discard_all()


# =================================================================================================


# =============================list_files==========================================================
def test_list_files_empty(mocker):
    mock_cache = mocker.MagicMock()
    mock_cache.fsm = Filesystem()
    mock_cache.root = Path("root_folder")
    mock_cache.pattern_list = ["*.ext", "*.map"]
    rglob_mock = mocker.patch("pathlib.Path.rglob", return_value=[])

    storage = Storage(mock_cache, removable=True)

    assert not storage.list_files()
    rglob_mock.assert_has_calls([call("*.ext"), call("*.map")])


def test_list_files_all(mocker):
    mock_cache = mocker.MagicMock()
    mock_cache.fsm = Filesystem()
    mock_cache.root = Path("root_folder")
    mock_cache.pattern_list = ["*.ext"]

    paths = [
        Path("folder/subfolder/file"),
        Path("folder/.subfolder/subfolder/file"),
    ]
    for path in paths:
        path.parent.mkdir(exist_ok=True, parents=True)
        open(path, "w", encoding="utf-8")  # pylint: disable=consider-using-with

    rglob_mock = mocker.patch(
        "pathlib.Path.rglob",
        return_value=[
            Path("folder"),
            Path("folder/subfolder"),
            Path("folder/.subfolder/subfolder/file"),
        ],
    )

    storage = Storage(mock_cache, removable=True)

    assert storage.list_files() == [
        Path("folder"),
        Path("folder/subfolder"),
    ]
    rglob_mock.assert_called_once_with("*.ext")
    rmtree("folder")


def test_list_files_depth(mocker):
    mock_cache = mocker.MagicMock()
    mock_cache.fsm = Filesystem()
    mock_cache.root = Path("root_folder")
    mock_cache.pattern_list = ["*.ext"]

    paths = [
        Path("folder/file"),
        Path("folder/subfolder/file"),
        Path("folder/subfolder/subfolder/file"),
    ]
    for path in paths:
        path.parent.mkdir(exist_ok=True, parents=True)
        open(path, "w", encoding="utf-8")  # pylint: disable=consider-using-with

    rglob_mock = mocker.patch(
        "pathlib.Path.rglob",
        return_value=[
            Path("folder"),
            Path("folder/subfolder"),
            Path("folder/file"),
            Path("folder/subfolder/file"),
            Path("folder/subfolder/subfolder"),
            Path("folder/subfolder/subfolder/file"),
        ],
    )

    storage = Storage(mock_cache, removable=True)

    assert storage.list_files(2) == [
        Path("folder"),
        Path("folder/subfolder"),
        Path("folder/file"),
    ]
    rglob_mock.assert_called_once_with("*.ext")
    rmtree("folder")
