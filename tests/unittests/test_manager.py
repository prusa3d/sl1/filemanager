from pathlib import Path
from unittest.mock import call

import pytest
import pyudev
from PySignal import Signal

from filemanager.model import PrinterModel
from filemanager.errors import FileNotFound
from filemanager import defines, usb
from filemanager import manager


@pytest.fixture(name="local_storage")
def fixture_local_storage(mocker):
    cache_mock = mocker.MagicMock(spec=manager.Cache)
    type(cache_mock).name = mocker.PropertyMock(return_value="local")
    type(cache_mock).root = mocker.PropertyMock(return_value=Path("root"))
    mocker.patch("filemanager.manager.Cache", return_value=cache_mock)

    storage_mock = mocker.MagicMock(spec=manager.Storage)
    type(storage_mock).cache = mocker.PropertyMock(return_value=cache_mock)
    type(storage_mock).extensions = mocker.PropertyMock(return_value=[".ext"])
    type(storage_mock).is_removable = mocker.PropertyMock(return_value=False)
    mocker.patch("filemanager.manager.Storage", return_value=storage_mock)

    yield storage_mock


@pytest.fixture(name="usb_storage")
def fixture_usb_storage(mocker):
    cache_mock = mocker.MagicMock(spec=manager.Cache)
    type(cache_mock).value = mocker.PropertyMock(return_value="usb")
    type(cache_mock).root = mocker.PropertyMock(return_value=Path("usb"))
    mocker.patch("filemanager.manager.Cache", return_value=cache_mock)

    storage_mock = mocker.MagicMock(spec=manager.Storage)
    type(storage_mock).cache = mocker.PropertyMock(return_value=cache_mock)
    type(storage_mock).extensions = mocker.PropertyMock(return_value=[".ext"])
    type(storage_mock).is_removable = mocker.PropertyMock(return_value=True)
    mocker.patch("filemanager.manager.Storage", return_value=storage_mock)

    yield storage_mock


@pytest.fixture(name="previous_prints_storage")
def fixture_previous_prints_storage(mocker, previous_prints_filesystem):
    cache_mock = mocker.MagicMock(spec=manager.Cache)
    type(cache_mock).value = mocker.PropertyMock(return_value="previous-prints")
    type(cache_mock).root = mocker.PropertyMock(return_value=previous_prints_filesystem)
    mocker.patch("filemanager.manager.Cache", return_value=cache_mock)

    storage_mock = mocker.MagicMock(spec=manager.Storage)
    type(storage_mock).cache = mocker.PropertyMock(return_value=cache_mock)
    type(storage_mock).extensions = mocker.PropertyMock(return_value=[".ext"])
    type(storage_mock).is_removable = mocker.PropertyMock(return_value=False)
    mocker.patch("filemanager.manager.Storage", return_value=storage_mock)

    yield storage_mock


@pytest.fixture(name="udev_monitor")
def fixture_udev_monitor(mocker):
    udev_monitor_mock = mocker.MagicMock(spec=pyudev.MonitorObserver)
    monitor_mock = mocker.MagicMock(spec=usb.WriteMonitor)
    type(monitor_mock).busy_changed = Signal()
    mocker.patch("filemanager.manager.usb.monitor_factory", new=udev_monitor_mock)
    mocker.patch("filemanager.manager.execute", return_value=True)
    mocker.patch("filemanager.manager.usb.WriteMonitor", return_value=monitor_mock)
    yield udev_monitor_mock


# =========================filemanager.initialization==============================================
def test_filemanager_initialization_no_printer_model(udev_monitor):
    udev_monitor.return_value.is_alive.return_value = False

    with pytest.raises(Exception):
        manager.Manager(PrinterModel.NONE)

    udev_monitor.assert_called_once()


def test_filemanager_initialization_sl1s_printer_model(mocker, local_storage, udev_monitor):
    type(local_storage.cache).last_update = mocker.PropertyMock(return_value=1.0)
    type(local_storage.cache).name = mocker.PropertyMock(return_value="test")

    mocker.patch("filemanager.manager.usb.inserted", return_value=Path("/dev/sda1"))

    filemanager = manager.Manager(PrinterModel.SL1S)

    assert len(filemanager.storages) == 2
    assert len(filemanager.locals) == 2
    assert len(filemanager.removable) == 0
    assert len(filemanager.storages) == 2
    assert filemanager.last_update == {"test": 1.0}
    assert filemanager.removable_presented == "sda1"
    udev_monitor.assert_called_once()


# =================================================================================================


# =========================filemanager.start/stop monitor==========================================
def test_usb_already_inserted_before_manager_started(
    mocker, local_storage, udev_monitor
):  # pylint: disable=unused-argument
    """
    Test that the manager correctly handles the case when a USB device is already
    inserted before the manager is started. It should be already mounted -> insert_usb should be called.
    """
    mocker.patch("filemanager.manager.Cache")
    monitor_observer_mock = mocker.MagicMock(spec="pyudev.MonitorObserver")
    monitor_observer_mock.return_value.is_alive.return_value = False
    monitor_observer_mock.return_value.start.return_value = None
    mocker.patch("filemanager.manager.usb.monitor_factory", new=monitor_observer_mock)
    mocker.patch("filemanager.manager.execute", return_value=True)
    mocker.patch("filemanager.manager.usb.WriteMonitor")
    mocker.patch("filemanager.manager.Manager.is_mounted", return_value=True)
    mocker.patch("filemanager.manager.usb.inserted", return_value=Path("/dev/sda1"))
    device_mock = mocker.MagicMock(spec=pyudev.Device)
    device_mock.device_node = "/dev/sda1"
    mocker.patch("filemanager.manager.usb.inserted_device", return_value=device_mock)
    insert_usb_mock = mocker.patch("filemanager.manager.Manager.insert_usb")
    wait_mock = mocker.patch("filemanager.filesystem.Filesystem.wait_until", return_value=True)
    mocker.patch("filemanager.usb.Blktrace", autospec=True)

    filemanager = manager.Manager(PrinterModel.SL1S)

    wait_mock.assert_called_once()
    insert_usb_mock.assert_called_once_with(defines.USB_MOUNT_POINT / "sda1")
    assert filemanager.removable_presented == "sda1"
    monitor_observer_mock.return_value.start.assert_called_once()


def test_filemanager_start_monitor(udev_monitor):
    udev_monitor.return_value.is_alive.return_value = False

    # Monitor is started within the constructor
    filemanager = manager.Manager(PrinterModel.SL1S)  # pylint: disable=unused-variable

    # Check that the monitor is indeed started
    assert udev_monitor.return_value.mock_calls == [
        call.is_alive(),
        call.start(),
    ]


def test_filemanager_stop_monitor(udev_monitor):
    udev_monitor.return_value.is_alive.return_value = True

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.stop_monitor()

    # Check that the monitor has been running(thus was not started) and then stopped
    assert udev_monitor.return_value.mock_calls == [
        call.is_alive(),
        call.is_alive(),
        call.stop(),
    ]


# =================================================================================================


# =========================filemanager.insert/remove usb===========================================
def test_filemanager_insert_usb(mocker):
    mocker.patch("filemanager.manager.Cache")

    storage_mock = mocker.MagicMock()
    type(storage_mock.return_value).is_removable = mocker.PropertyMock(return_value=True)
    mocker.patch("filemanager.manager.Storage", new=storage_mock)

    monitor_observer_mock = mocker.MagicMock(spec="pyudev.MonitorObserver")
    monitor_observer_mock.return_value.is_alive.return_value = True
    mocker.patch("filemanager.manager.usb.monitor_factory", new=monitor_observer_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.insert_usb(path=Path("usb1"))

    assert len(filemanager.storages) == 4
    assert len(filemanager.locals) == 0
    assert len(filemanager.removable) == 4
    assert len(filemanager.storages) == 4

    filemanager.remove_usb()

    assert len(filemanager.storages) == 0
    assert len(filemanager.locals) == 0
    assert len(filemanager.removable) == 0
    assert len(filemanager.storages) == 0


def test_filemanager_remove_usb(mocker):
    mocker.patch("filemanager.manager.Cache")

    storage_mock = mocker.MagicMock()
    type(storage_mock.return_value).is_removable = mocker.PropertyMock(return_value=True)
    mocker.patch("filemanager.manager.Storage", new=storage_mock)

    monitor_observer_mock = mocker.MagicMock(spec="pyudev.MonitorObserver")
    monitor_observer_mock.return_value.is_alive.return_value = False
    mocker.patch("filemanager.manager.usb.monitor_factory", new=monitor_observer_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)

    assert len(filemanager.storages) == 2
    assert len(filemanager.locals) == 0
    assert len(filemanager.removable) == 2
    assert len(filemanager.storages) == 2

    filemanager.remove_usb()

    assert len(filemanager.storages) == 0
    assert len(filemanager.locals) == 0
    assert len(filemanager.removable) == 0
    assert len(filemanager.storages) == 0


# =================================================================================================


# =========================filemanager.path_to_storage=============================================
def test_filemanager_path_to_storage_type_error():
    filemanager = manager.Manager(PrinterModel.SL1S)
    with pytest.raises(FileNotFoundError):
        filemanager.path_to_storage(path=None)


def test_filemanager_path_to_storage_not_found(mocker):
    mocker.patch("pathlib.Path.is_dir", return_value=False)

    filemanager = manager.Manager(PrinterModel.SL1S)

    assert len(filemanager.storages) == 2
    with pytest.raises(FileNotFoundError):
        filemanager.path_to_storage(path=Path("not_exists"))


def test_filemanager_path_to_storage_found(mocker, local_storage):
    mocker.patch("filemanager.filesystem.Filesystem.is_parent", return_value=True)

    filemanager = manager.Manager(PrinterModel.SL1S)

    assert len(filemanager.storages) == 2
    assert filemanager.path_to_storage(path=Path("not_exists")) == local_storage


# =================================================================================================


# =========================filemanager.path_has_storage============================================
def test_filemanager_path_has_storage_false(mocker):
    mocker.patch("filemanager.manager.Manager.path_to_storage", side_effect=FileNotFoundError())

    filemanager = manager.Manager(PrinterModel.SL1S)

    assert not filemanager.path_has_storage(path=Path("noneexistence"))


def test_filemanager_path_has_storage_true(mocker):
    mocker.patch("filemanager.filesystem.Filesystem.is_parent", return_value=True)

    filemanager = manager.Manager(PrinterModel.SL1S)

    assert filemanager.path_has_storage(path=Path("valid"))


# =================================================================================================


# =========================filemanager.current_project=============================================
def test_filemanager_clean_project(mocker):
    clean_mock = mocker.patch("filemanager.manager.Manager.clean_current_project")

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.current_project = None
    clean_mock.assert_called_once()


def test_filemanager_set_project_not_in_storage(mocker):
    mocker.patch("filemanager.filesystem.Filesystem.is_parent", return_value=False)
    mocker.patch("filemanager.manager.Manager.clean_current_project")

    filemanager = manager.Manager(PrinterModel.SL1S)

    with pytest.raises(FileNotFound):
        filemanager.current_project = Path("project")


def test_filemanager_set_project_symlink_err(mocker, local_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)
    mocker.patch("filemanager.manager.Manager.clean_current_project")
    mocker.patch("pathlib.Path.symlink_to", side_effect=FileNotFoundError())

    filemanager = manager.Manager(PrinterModel.SL1S)

    with pytest.raises(FileNotFound):
        filemanager.current_project = Path("project")


def test_filemanager_set_project_symlink(mocker, local_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)
    symlink_mock = mocker.patch("pathlib.Path.symlink_to")
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path("project"))
    mocker.patch("filemanager.manager.Manager.clean_current_project")

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.current_project = Path("project")

    symlink_mock.assert_called_once_with(Path("project"))


def test_filemanager_set_project_removable_insuficient_size(mocker, usb_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=usb_storage)
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path("project"))
    mocker.patch("filemanager.filesystem.Filesystem.available_storage", return_value=(50, 20, 30))
    mocker.patch("filemanager.manager.Manager.clean_current_project")

    stat_mock = mocker.MagicMock()
    type(stat_mock).st_size = mocker.PropertyMock(return_value=1000)
    mocker.patch("pathlib.Path.stat", return_value=stat_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)

    with pytest.raises(IOError):
        filemanager.current_project = Path("project")


def test_filemanager_set_project_removable(mocker, usb_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=usb_storage)
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path("project"))
    mocker.patch("filemanager.filesystem.Filesystem.available_storage", return_value=(50, 20, 30))
    mocker.patch("filemanager.manager.Manager.clean_current_project")
    mocker.patch("pathlib.Path.read_bytes")
    mocker.patch("pathlib.Path.write_bytes")
    move_mock = mocker.patch("filemanager.filesystem.Filesystem.move")
    cmo_mock = mocker.patch("filemanager.filesystem.Filesystem.ch_mode_owner")

    stat_mock = mocker.MagicMock()
    type(stat_mock).st_size = mocker.PropertyMock(return_value=30)
    mocker.patch("pathlib.Path.stat", return_value=stat_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.current_project = Path("project")

    move_mock.assert_called_once_with(defines.PROJECT_SYMLINK / "project~", defines.PROJECT_SYMLINK / "project")
    cmo_mock.assert_called_once_with(defines.PROJECT_SYMLINK / "project", defines.PROJECT_GROUP, defines.PROJECT_MODE)


def test_filemanager_get_project_none(mocker, previous_prints_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=previous_prints_storage)
    mocker.patch("filemanager.manager.Storage.list_files", return_value=[])

    filemanager = manager.Manager(PrinterModel.SL1S)

    assert filemanager.current_project == Path("")


def test_filemanager_get_project(mocker, previous_prints_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=previous_prints_storage)
    previous_prints_storage.list_files = mocker.MagicMock(return_value=[Path("project")])
    filemanager = manager.Manager(PrinterModel.SL1S)

    assert filemanager.current_project == Path("project")


# =================================================================================================


# =========================filemanager.get_project_details=========================================
# pylint: disable=pointless-string-statement
"""
def test_filemanager_project_details(mocker):
    source_mock = mocker.MagicMock()
    type(source_mock.return_value).value = mocker.PropertyMock(return_value="test")
    mocker.patch("filemanager.manager.Source", new=source_mock)

    cache_mock = mocker.MagicMock()
    type(cache_mock.return_value).exists = mocker.MagicMock(return_value=False)
    type(cache_mock.return_value).create = mocker.MagicMock()
    type(cache_mock.return_value).get_config = mocker.MagicMock(return_value="config")
    type(cache_mock.return_value).get_thumbnails = mocker.MagicMock(return_value="thumbnail")
    mocker.patch("filemanager.manager.Cache", new=cache_mock)

    storage_mock = mocker.MagicMock()
    type(storage_mock.return_value).source = mocker.PropertyMock(return_value=source_mock.return_value)
    type(storage_mock.return_value).cache = mocker.PropertyMock(return_value=cache_mock.return_value)
    mocker.patch("filemanager.manager.Storage", new=storage_mock)

    mocker.patch("pyudev.MonitorObserver.is_alive", return_value=False)
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=storage_mock.return_value)

    stat_mock = mocker.MagicMock()
    type(stat_mock).st_size = mocker.PropertyMock(return_value=1000)
    type(stat_mock).st_mtime = mocker.PropertyMock(return_value=1000.0)
    type(stat_mock).st_ctime = mocker.PropertyMock(return_value=1100.0)
    mocker.patch("pathlib.Path.stat", return_value=stat_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)
    assert filemanager.project_details(path=Path("project_details")) == {
        "path": "project_details",
        "metadata": {
            "config": "config",
            "thumbnail": "thumbnail",
        },
        "origin": "test",
        "type": "file",
        "size": 1000,
        "mtime": 1100.0,
        "exists": True,
    }
"""

# =================================================================================================


# =========================filemanager.move========================================================
def test_filemanager_move_not_storage(mocker, local_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)

    filemanager = manager.Manager(PrinterModel.SL1S)

    with pytest.raises(FileNotFoundError):
        filemanager.move(src=Path("noneexistence"), dest=Path("unused"))


def test_filemanager_move_to_local_storage(mocker, local_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)
    move_mocker = mocker.patch("filemanager.filesystem.Filesystem.move")
    cmo_mock = mocker.patch("filemanager.filesystem.Filesystem.ch_mode_owner")

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.move(Path("source"), Path("destination"))

    move_mocker.assert_called_once_with(Path("source"), Path("destination"))
    cmo_mock.assert_called_once()


# =================================================================================================


# =========================filemanager.remove======================================================
def test_filemanager_remove_permission_denied(mocker, local_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)

    filemanager = manager.Manager(PrinterModel.SL1S)

    with pytest.raises(PermissionError):
        filemanager.remove(path=Path("root"))


def test_filemanager_remove_pass(mocker, local_storage):
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)
    remove_path_mock = mocker.patch("filemanager.filesystem.Filesystem.remove_path")

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.remove(Path("valid"))

    remove_path_mock.assert_called_once_with(Path("valid"), True)


# ======================has_deprecated test section================================================
def test_has_deprecated_false_no_files(mocker, local_storage):
    list_files_mock = mocker.patch("filemanager.filesystem.Filesystem.list_files", return_value=[])

    filemanager = manager.Manager(PrinterModel.SL1S)

    assert not filemanager.has_deprecated(storage=local_storage)
    list_files_mock.assert_called_once_with(Path("root"), PrinterModel.SL1S.extensions_deprecated)


def test_has_deprecated_true(mocker, local_storage):
    list_files_mock = mocker.patch("filemanager.filesystem.Filesystem.list_files", return_value=["a.dwz"])

    filemanager = manager.Manager(PrinterModel.SL1S)

    assert filemanager.has_deprecated(local_storage)
    list_files_mock.assert_called_once_with(Path("root"), PrinterModel.SL1S.extensions_deprecated)


# =================================================================================================


# ===============================archive_deprecated test section===================================
def test_archive_deprecated(mocker, local_storage):
    mocker.patch("filemanager.filesystem.Filesystem.list_files", return_value=[Path("root/a.dwz")])
    mocker.patch("pathlib.Path.exists", return_value=False)
    mocker.patch("pathlib.Path.mkdir")
    move_mock = mocker.patch("filemanager.filesystem.Filesystem.move")

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.archive_deprecated(local_storage)

    move_mock.assert_called_once_with(Path("root/a.dwz"), defines.OLD_PROJECTS_DOWNLOAD / "a.dwz")


def test_archive_deprecated_already_exists(mocker, local_storage):
    mocker.patch("filemanager.filesystem.Filesystem.list_files", return_value=[Path("root/a.dwz")])
    mocker.patch("pathlib.Path.exists", side_effect=[True, False])
    mocker.patch("pathlib.Path.mkdir")
    move_mock = mocker.patch("filemanager.filesystem.Filesystem.move")

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.archive_deprecated(local_storage)

    move_mock.assert_called_once_with(Path("root/a.dwz"), defines.OLD_PROJECTS_DOWNLOAD / "a_copy1.dwz")


# =================================================================================================


# ===============================usb_event_handler test section===================================
def test_usb_event_handler_device_not_fs(mocker):
    device_mock = mocker.MagicMock(spec=pyudev.Device)
    get_mock = mocker.MagicMock(return_value=False)
    type(device_mock).get = get_mock

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.usb_event_handler(device=device_mock)

    get_mock.assert_called_once()


def test_usb_event_handler_remove(mocker):
    device_mock = mocker.MagicMock(spec=pyudev.Device)
    type(device_mock).get = mocker.MagicMock(return_value=True)
    type(device_mock).action = mocker.PropertyMock(return_value="remove")
    remove_usb_mock = mocker.patch("filemanager.manager.Manager.remove_usb")

    write_monitor_mock = mocker.patch("filemanager.usb.WriteMonitor")
    type(write_monitor_mock).busy_changed = Signal()

    filemanager = manager.Manager(PrinterModel.SL1S)

    filemanager.usb_event_handler(device=device_mock)

    remove_usb_mock.assert_called_once()


def test_usb_event_handler_add_timeout(mocker):
    device_mock = mocker.MagicMock(spec=pyudev.Device)
    type(device_mock).get = mocker.MagicMock(return_value=True)
    type(device_mock).action = mocker.PropertyMock(return_value="add")
    mocker.patch("filemanager.usb.Blktrace")
    mocker.patch("pathlib.Path.exists", return_value=False)
    insert_usb_mock = mocker.patch("filemanager.manager.Manager.insert_usb")
    mocker.patch("filemanager.manager.execute", return_value=True)

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.usb_event_handler(device=device_mock)

    insert_usb_mock.assert_not_called()


def test_usb_event_handler_add_pass(mocker):
    device_mock = mocker.MagicMock(spec=pyudev.Device)
    type(device_mock).get = mocker.MagicMock(return_value=True)
    type(device_mock).action = mocker.PropertyMock(return_value="add")
    type(device_mock).device_node = mocker.PropertyMock(return_value="/dev/sda")
    wait_mock = mocker.patch("filemanager.filesystem.Filesystem.wait_until", return_value=True)
    insert_usb_mock = mocker.patch("filemanager.manager.Manager.insert_usb")
    mocker.patch("filemanager.usb.Blktrace", autospec=True)

    filemanager = manager.Manager(PrinterModel.SL1S)
    filemanager.usb_event_handler(device=device_mock)

    wait_mock.assert_called_once()
    insert_usb_mock.assert_called_once_with(defines.USB_MOUNT_POINT / "sda")


# =================================================================================================


# ===============================get_project test section==========================================
# pylint: disable=pointless-string-statement
"""
def test_get_project_is_hidden(mocker, local_storage):
    mocker.patch("filemanager.filesystem.Filesystem.is_hidden", return_value=True)
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)

    stat_mock = mocker.MagicMock()
    type(stat_mock).st_mtime = mocker.PropertyMock(return_value=2.0)
    type(stat_mock).st_ctime = mocker.PropertyMock(return_value=1.0)
    mocker.patch("pathlib.Path.stat", return_value=stat_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)
    assert filemanager.get_project(path=Path("hidden"), depth=0) == {
        "path": "root",
        "origin": "local",
        "type": "folder",
        "mtime": 2.0,
        "children": [],
        "exists": False,
    }


def test_get_project_not_valid_project(mocker, local_storage):
    mocker.patch("filemanager.filesystem.Filesystem.is_hidden", return_value=False)
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)
    mocker.patch("pathlib.Path.is_dir", return_value=False)
    mocker.patch("filemanager.filesystem.Filesystem.is_valid_project", return_value=False)

    stat_mock = mocker.MagicMock()
    type(stat_mock).st_mtime = mocker.PropertyMock(return_value=2.0)
    type(stat_mock).st_ctime = mocker.PropertyMock(return_value=1.0)
    mocker.patch("pathlib.Path.stat", return_value=stat_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)
    assert filemanager.get_project(path=Path("hidden"), depth=1) == {
        "path": "root",
        "origin": "local",
        "type": "folder",
        "mtime": 2.0,
        "children": [],
        "exists": False,
    }


def test_get_project_pass(mocker, local_storage):
    mocker.patch("filemanager.filesystem.Filesystem.is_hidden", return_value=False)
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)
    mocker.patch("filemanager.manager.Manager.project_details", return_value={"path": "valid", "mtime": 100})
    mocker.patch("pathlib.Path.is_dir", side_effect=[True, False])
    mocker.patch("pathlib.Path.iterdir", return_value=[Path("valid_project.ext")])
    mocker.patch("filemanager.filesystem.Filesystem.is_valid_project", return_value=True)

    stat_mock = mocker.MagicMock()
    type(stat_mock).st_mtime = mocker.PropertyMock(return_value=1000.0)
    type(stat_mock).st_ctime = mocker.PropertyMock(return_value=1100.0)
    mocker.patch("pathlib.Path.stat", return_value=stat_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)
    assert filemanager.get_project(path=Path("folder"), depth=2) == {
        "children": [
            {
                "mtime": 100,
                "path": "valid",
            }
        ],
        "exists": True,
        "mtime": 1100.0,
        "origin": "local",
        "path": "folder",
        "type": "folder",
    }


def test_get_project_empty_folder(mocker, local_storage):
    mocker.patch("filemanager.filesystem.Filesystem.is_hidden", return_value=False)
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)
    mocker.patch("pathlib.Path.is_dir", side_effect=[True, True])
    mocker.patch("pathlib.Path.iterdir", return_value=[])

    stat_mock = mocker.MagicMock()
    type(stat_mock).st_mtime = mocker.PropertyMock(return_value=1000.0)
    type(stat_mock).st_ctime = mocker.PropertyMock(return_value=1100.0)
    mocker.patch("pathlib.Path.stat", return_value=stat_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)
    assert filemanager.get_project(path=Path("folder"), depth=2) == {
        "children": [],
        "exists": True,
        "mtime": 1100.0,
        "origin": "local",
        "path": "folder",
        "type": "folder",
    }


def test_get_project_no_extension(mocker, local_storage):
    mocker.patch("filemanager.filesystem.Filesystem.is_hidden", return_value=False)
    mocker.patch("filemanager.manager.Manager.path_to_storage", return_value=local_storage)
    mocker.patch("filemanager.manager.Manager.project_details", return_value={"path": "valid", "mtime": 100})
    mocker.patch("pathlib.Path.is_dir", side_effect=[True, False])
    mocker.patch("pathlib.Path.iterdir", return_value=[Path("valid_project")])
    mocker.patch("filemanager.filesystem.Filesystem.is_valid_project", return_value=True)

    stat_mock = mocker.MagicMock()
    type(stat_mock).st_mtime = mocker.PropertyMock(return_value=1000.0)
    type(stat_mock).st_ctime = mocker.PropertyMock(return_value=1100.0)
    mocker.patch("pathlib.Path.stat", return_value=stat_mock)

    filemanager = manager.Manager(PrinterModel.SL1S)
    assert filemanager.get_project(path=Path("folder"), depth=2) == {
        "children": [],
        "exists": True,
        "mtime": 1100.0,
        "origin": "local",
        "path": "folder",
        "type": "folder",
    }
"""
