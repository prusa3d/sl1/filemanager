from pathlib import Path
from unittest.mock import create_autospec, mock_open, call
from hashlib import md5
from dataclasses import dataclass

from PySignal import Signal

from filemanager.cache import CachedFolder, CachedFile, Cache
from filemanager.filesystem import Filesystem
from filemanager import defines

# pylint: disable=protected-access


# ======================== cached items ===========================================================
def test_cached_folder():
    cached_folder = CachedFolder("any_folder", "xy", 48.48, [CachedFile("any_file", "ab", 128.128, 1024)])
    assert cached_folder.asdict() == {
        "exists": True,
        "path": "any_folder",
        "mtime": 48.48,
        "children": [
            {
                "exists": True,
                "path": "any_file",
                "mtime": 128.128,
                "size": 1024,
                "type": "file",
                "origin": "ab",
            }
        ],
        "type": "folder",
        "origin": "xy",
    }


def test_cached_file():
    chached_file = CachedFile("any_file", "xy", 128.128, 1024)
    assert chached_file.asdict() == {
        "exists": True,
        "path": "any_file",
        "mtime": 128.128,
        "size": 1024,
        "type": "file",
        "origin": "xy",
    }


def test_cached_file_no_metadata():
    chached_file = CachedFile("any_file", "xy", 128.128, 1024, {})
    assert chached_file.asdict() == {
        "exists": True,
        "path": "any_file",
        "mtime": 128.128,
        "size": 1024,
        "type": "file",
        "origin": "xy",
    }


def test_cached_file_with_metadata():
    chached_file = CachedFile("any_file", "xy", 128.128, 1024, {"1": {"x": "yy"}})
    assert chached_file.asdict() == {
        "exists": True,
        "path": "any_file",
        "mtime": 128.128,
        "size": 1024,
        "metadata": {"1": {"x": "yy"}},
        "type": "file",
        "origin": "xy",
    }


# =================================================================================================


# ======================== cache.initialization ===================================================
def test_cache_initialization(mocker):
    mocker.patch("pathlib.Path.mkdir", return_value=None)
    mocker.patch("pathlib.Path.stat", return_value=Stat(defines.PROJECT_MODE, 1, 1.1, 2.2))

    on_update = Signal()
    name = "test"
    root = Path("/test")
    cache = Cache(name, root, ["ext"], Filesystem(), on_update)

    assert (
        str(cache)
        == f"Cache name={name} root={root} pattern_list=['*.ext'] cache_dir={defines.CACHE_DIR / name} item_count=1"
    )


# =================================================================================================


# ======================== cache add get remove ===================================================
@dataclass
class Stat:
    "mock structure"
    st_mode: int
    st_size: int
    st_mtime: float
    st_ctime: float


def test_cache_add_get_remove_dir(mocker):
    def handle(_):
        pass

    name = "test"
    root = Path("/test")
    item = root / "item"
    mocker.patch("pathlib.Path.is_dir", return_value=True)
    mocker.patch("pathlib.Path.stat", return_value=Stat(defines.PROJECT_MODE, 1, 1.1, 2.2))
    mkdir_mock = mocker.patch("pathlib.Path.mkdir", return_value=None)
    remove_path_mock = mocker.patch("filemanager.filesystem.Filesystem.remove_path")
    even_mock = create_autospec(handle)

    on_update = Signal()
    on_update.connect(even_mock)
    cache = Cache(name, root, [], Filesystem(), on_update)

    cache.add(item)
    even_mock.assert_called_once()
    even_mock.reset_mock()

    data = cache.get(item)
    assert data.asdict() == {
        "exists": True,
        "path": str(item),
        "mtime": 2.2,
        "children": [],
        "type": "folder",
        "origin": name,
    }
    mkdir_mock.assert_not_called()

    cache.remove(item)
    even_mock.assert_called_once()
    cache_folder_path = defines.CACHE_DIR / name / md5(str(item).encode("utf-8")).hexdigest()
    remove_path_mock.assert_called_once_with(cache_folder_path, recursive=True)


def test_cache_add_get_remove_file(mocker):
    def handle(_):
        pass

    name = "test"
    root = Path("/test")
    item = root / "item"
    mocker.patch("pathlib.Path.is_dir", return_value=False)
    mocker.patch("pathlib.Path.stat", return_value=Stat(defines.PROJECT_MODE, 32768, 1.1, 2.2))
    mkdir_mock = mocker.patch("pathlib.Path.mkdir", return_value=None)
    remove_path_mock = mocker.patch("filemanager.filesystem.Filesystem.remove_path")
    even_mock = create_autospec(handle)

    on_update = Signal()
    on_update.connect(even_mock)
    cache = Cache(name, root, [], Filesystem(), on_update)

    cache.add(item)
    even_mock.assert_called_once()
    even_mock.reset_mock()

    data = cache.get(item)
    assert data.asdict() == {
        "exists": True,
        "path": str(item),
        "mtime": 2.2,
        "size": 32768,
        "type": "file",
        "origin": name,
    }
    mkdir_mock.assert_not_called()

    cache.remove(item)
    even_mock.assert_called_once()
    cache_folder_path = defines.CACHE_DIR / name / md5(str(item).encode("utf-8")).hexdigest()
    remove_path_mock.assert_called_once_with(cache_folder_path, recursive=True)


def test_cache_add_get_remove_file_metadata(mocker):
    def handle(_):
        pass

    name = "test"
    root = Path("/test")
    item = root / "item"
    mocker.patch("pathlib.Path.is_dir", return_value=False)
    mocker.patch("pathlib.Path.stat", return_value=Stat(defines.PROJECT_MODE, 32768, 1.1, 2.2))
    mocker.patch("pathlib.Path.glob", return_value=[Path("thumbnail1"), Path("thumbnail2")])
    mocker.patch("builtins.open", mock_open(read_data="item = value"))
    mocker.patch("filemanager.filesystem.Filesystem.is_valid_project", return_value=True)
    mkdir_mock = mocker.patch("pathlib.Path.mkdir", return_value=None)
    remove_path_mock = mocker.patch("filemanager.filesystem.Filesystem.remove_path")
    cmo_mock = mocker.patch("filemanager.filesystem.Filesystem.ch_mode_owner")
    extract_project_mock = mocker.patch("filemanager.filesystem.Filesystem.extract_project_info")
    even_mock = create_autospec(handle)

    on_update = Signal()
    on_update.connect(even_mock)
    cache = Cache(name, root, [], Filesystem(), on_update)

    cache.add(item)
    even_mock.assert_called_once()
    even_mock.reset_mock()

    cache_folder_path = defines.CACHE_DIR / name / md5(str(item).encode("utf-8")).hexdigest()
    data = cache.get(item, with_metadata=True)
    assert data.asdict() == {
        "exists": True,
        "path": str(item),
        "mtime": 2.2,
        "size": 32768,
        "metadata": {
            "config": {"item": "value"},
            "thumbnail": {
                "dir": str(cache_folder_path),
                "files": [
                    str(cache_folder_path / "thumbnail1"),
                    str(cache_folder_path / "thumbnail2"),
                ],
            },
        },
        "type": "file",
        "origin": name,
    }
    mkdir_mock.assert_called_once_with(parents=True, exist_ok=True)
    cmo_mock.assert_called_once_with(cache_folder_path, defines.PROJECT_GROUP, defines.PROJECT_DIR_CACHE)
    extract_project_mock.assert_called_once_with(item, cache_folder_path, defines.PROJECT_CONFIG_FILE)

    cache.remove(item)
    even_mock.assert_called_once()
    remove_path_mock.assert_called_once_with(cache_folder_path, recursive=True)


def test_cache_add_get_remove_tree(mocker):
    def handle(_):
        pass

    name = "tree"
    root = Path("/test")
    tree = [root / "file1", root / "dir" / "file2"]
    mocker.patch("pathlib.Path.is_dir", side_effect=[True, False, True, False])
    mocker.patch("pathlib.Path.stat", return_value=Stat(defines.PROJECT_MODE, 1, 1.1, 2.2))
    mkdir_mock = mocker.patch("pathlib.Path.mkdir", return_value=None)
    remove_path_mock = mocker.patch("filemanager.filesystem.Filesystem.remove_path")
    even_mock = create_autospec(handle)

    on_update = Signal()
    on_update.connect(even_mock)
    cache = Cache(name, root, [], Filesystem(), on_update)

    for item in tree:
        cache.add(item, notify=False)

    even_mock.assert_not_called()

    data = cache.get(root)
    assert data.asdict() == {
        "exists": True,
        "path": "/test",
        "mtime": 2.2,
        "type": "folder",
        "origin": name,
        "children": [
            {
                "exists": True,
                "path": "/test/file1",
                "mtime": 2.2,
                "size": 1,
                "type": "file",
                "origin": name,
            },
            {
                "exists": True,
                "path": "/test/dir",
                "mtime": 2.2,
                "type": "folder",
                "origin": name,
                "children": [
                    {
                        "exists": True,
                        "path": "/test/dir/file2",
                        "mtime": 2.2,
                        "size": 1,
                        "type": "file",
                        "origin": name,
                    }
                ],
            },
        ],
    }
    mkdir_mock.assert_not_called()

    for item in reversed(tree):
        cache.remove(item, notify=False)

    calls = []  # type: ignore
    for item in [root / "dir" / "file2", root / "dir", root / "file1"]:
        calls.append(call(defines.CACHE_DIR / name / md5(str(item).encode("utf-8")).hexdigest(), recursive=True))

    remove_path_mock.assert_has_calls(calls)
    even_mock.assert_not_called()


# =================================================================================================


# ======================== cache discard_all ======================================================
def test_cache_discard_all(mocker):
    def handle(_):
        pass

    name = "test"
    root = Path("/test")
    tree = [root / "file1", root / "dir" / "file2"]
    mocker.patch("pathlib.Path.is_dir", side_effect=[True, False, True, False])
    mocker.patch("pathlib.Path.stat", return_value=Stat(defines.PROJECT_MODE, 1, 1.1, 2.2))
    mocker.patch("pathlib.Path.mkdir", return_value=None)
    remove_path_mock = mocker.patch("filemanager.filesystem.Filesystem.remove_path")
    even_mock = create_autospec(handle)

    on_update = Signal()
    on_update.connect(even_mock)
    cache = Cache(name, root, [], Filesystem(), on_update)

    for item in tree:
        cache.add(item, notify=False)

    even_mock.assert_not_called()

    cache.discard_all()
    even_mock.assert_called_once()

    remove_path_mock.assert_called_once_with(defines.CACHE_DIR / name, recursive=True)
