from shutil import rmtree
import stat
from pathlib import Path
from unittest.mock import call
import tempfile

import pytest
import pyudev

from filemanager import usb
from filemanager.filesystem import Filesystem


# ========================test helpers section=====================================================


def walk_directory_func(value: Path):
    return not bool(value.suffix)


# =================================================================================================


# =========================helpers test section====================================================
def test_walk_directory_func_false():
    assert not walk_directory_func(Path("a.txt"))


def test_walk_directory_func_true():
    assert walk_directory_func(Path("."))


# =================================================================================================


# ========================normalize_path test section==============================================
def test_check_path_none(mocker):
    mocker.patch("sys.getfilesystemencoding", return_value="iso-8859-15")
    fsm = Filesystem()
    with pytest.raises(TypeError):
        fsm.normalize_path(None)
    with pytest.raises(TypeError):
        fsm.normalize_path(Path("a\u0411b"))
    with pytest.raises(TypeError):
        fsm.normalize_path("blahblah")  # type: ignore


def test_check_path_true():
    assert Filesystem().normalize_path(Path(".")) == Path(".").resolve()


# =================================================================================================


# =====================ch_mode_owner test section==================================================
def test_ch_mode_owner_file(mocker):
    chown_mock = mocker.patch("shutil.chown", return_value=None)
    chmod_mock = mocker.patch("pathlib.Path.chmod", return_value=None)
    mocker.patch("pathlib.Path.is_dir", return_value=False)
    mode = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH

    Filesystem().ch_mode_owner(Path("."), "super-users", mode)

    chown_mock.assert_called_once_with(Path(".").resolve(), group="super-users")
    chmod_mock.assert_called_once_with(mode)


def test_ch_mode_owner_empty_dir(mocker):
    chown_mock = mocker.patch("shutil.chown", return_value=None)
    chmod_mock = mocker.patch("pathlib.Path.chmod", return_value=None)
    mocker.patch("pathlib.Path.is_dir", return_value=True)
    mocker.patch("pathlib.Path.iterdir", return_value=[])
    mode = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH

    Filesystem().ch_mode_owner(Path("."), "super-users", mode)

    chown_mock.assert_called_once_with(Path(".").resolve(), group="super-users")
    chmod_mock.assert_called_once_with(mode)


def test_ch_mode_owner_inner_dir_file(mocker):
    chown_mock = mocker.patch("shutil.chown", return_value=None)
    chmod_mock = mocker.patch("pathlib.Path.chmod", return_value=None)
    mocker.patch("pathlib.Path.is_dir", side_effect=walk_directory_func)
    mocker.patch("pathlib.Path.iterdir", return_value=["a.txt"])
    mode = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH

    Filesystem().ch_mode_owner(Path("."), "super-users", mode)

    chown_mock.assert_has_calls(
        [
            call(Path(".").resolve(), group="super-users"),
            call(Path("./a.txt").resolve(), group="super-users"),
        ]
    )
    chmod_mock.assert_has_calls([call(mode), call(mode)])


# =================================================================================================


# =============================is_hidden===========================================================
def test_is_hidden_false():
    assert not Filesystem().is_hidden(Path("."))


def test_is_hidden_true():
    fsm = Filesystem()
    assert fsm.is_hidden(Path(".hidden"))
    assert fsm.is_hidden(False)  # type: ignore


def test_is_hidden_nasted_true():
    assert Filesystem().is_hidden(Path("dir/.hidden/true"))


# =================================================================================================


# =============================is_parent===========================================================
def test_is_parent_true():
    fsm = Filesystem()
    assert fsm.is_parent(Path("parent/child"), Path("parent"))
    assert fsm.is_parent(Path("/same"), Path("/same"))


def test_is_parent_false():
    fsm = Filesystem()
    assert not fsm.is_parent(Path("wrong/child"), Path("parent"))
    assert not fsm.is_parent(Path("wrong/child"), None)


# =================================================================================================


# =============================remove_path=========================================================
def test_remove_path_directory(mocker):
    unlink_mock = mocker.patch("pathlib.Path.unlink", return_value=None)
    rmdir_mock = mocker.patch("pathlib.Path.rmdir", return_value=None)
    rmtree_mock = mocker.patch("shutil.rmtree", return_value=None)
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path("folder"))
    mocker.patch("filemanager.filesystem.Filesystem.is_mount", return_value=False)
    mocker.patch("pathlib.Path.is_dir", return_value=True)
    mocker.patch("pathlib.Path.exists", return_value=True)

    Filesystem().remove_path(path=Path("folder"))

    unlink_mock.assert_not_called()
    rmdir_mock.assert_called_once()
    rmtree_mock.assert_not_called()


def test_remove_path_directory_recursive(mocker):
    unlink_mock = mocker.patch("pathlib.Path.unlink", return_value=None)
    rmdir_mock = mocker.patch("pathlib.Path.rmdir", return_value=None)
    rmtree_mock = mocker.patch("shutil.rmtree", return_value=None)
    mocker.patch("filemanager.filesystem.Filesystem.is_mount", return_value=False)
    mocker.patch("pathlib.Path.is_dir", return_value=True)
    mocker.patch("pathlib.Path.exists", return_value=True)
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path("folder"))

    Filesystem().remove_path(path=Path("folder"), recursive=True)

    unlink_mock.assert_not_called()
    rmdir_mock.assert_not_called()
    rmtree_mock.assert_called_once_with(Path("folder"))


def test_remove_path_file(mocker):
    unlink_mock = mocker.patch("pathlib.Path.unlink", return_value=None)
    rmdir_mock = mocker.patch("pathlib.Path.rmdir", return_value=None)
    mocker.patch("pathlib.Path.exists", return_value=True)
    rmtree_mock = mocker.patch("shutil.rmtree", return_value=None)
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path("file"))
    mocker.patch("filemanager.filesystem.Filesystem.is_mount", return_value=False)

    Filesystem().remove_path(path=Path("file"))

    unlink_mock.assert_called_once()
    rmdir_mock.assert_not_called()
    rmtree_mock.assert_not_called()


def test_remove_path_mounted_file(mocker):
    unlink_mock = mocker.patch("pathlib.Path.unlink", return_value=None)
    mocker.patch("pathlib.Path.exists", return_value=True)
    mocker.patch("filemanager.filesystem.Filesystem.is_mount", return_value=True)
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path("file"))

    Filesystem().remove_path(path=Path("./file"))

    unlink_mock.assert_called_once()


def test_remove_path_no_exists(mocker):
    unlink_mock = mocker.patch("pathlib.Path.unlink", return_value=None)
    rmdir_mock = mocker.patch("pathlib.Path.rmdir", return_value=None)
    rmtree_mock = mocker.patch("shutil.rmtree", return_value=None)

    Filesystem().remove_path(path=Path("nonexistence"))

    unlink_mock.assert_not_called()
    rmdir_mock.assert_not_called()
    rmtree_mock.assert_not_called()


# =================================================================================================


# ==========is valid project=======================================================================
def test_is_valid_project_not_zipfile(mocker):
    is_zipfile_mock = mocker.patch("zipfile.is_zipfile", return_value=False)
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path("not_a_zip"))

    assert not Filesystem().is_valid_project(Path("not_a_zip"))
    is_zipfile_mock.assert_called_once_with("not_a_zip")


def test_is_valid_project_not_file():
    assert not Filesystem().is_valid_project(path=None)


def test_is_valid_project_not_contain_config(mocker):
    mock_zip_file = mocker.MagicMock()
    mocker.patch("zipfile.ZipFile", new=mock_zip_file)
    mocker.patch("zipfile.is_zipfile", return_value=True)

    assert not Filesystem().is_valid_project(Path("not_a_zip"))


def test_is_valid_project_true(mocker):
    mocker.patch("zipfile.is_zipfile", return_value=True)
    mock_zip_file = mocker.MagicMock()
    mocker.patch("zipfile.ZipFile", new=mock_zip_file)
    mock_zip_file.return_value.namelist.return_value = ["config.ini"]

    assert Filesystem().is_valid_project(Path("zip"), metadata_file="config.ini")


# =================================================================================================


# ==========extract project info===================================================================
def test_extract_project_info_no_thumbnails(mocker):
    mock_zip_file = mocker.MagicMock()
    mock_zip_file.return_value.__enter__.return_value = mock_zip_file.return_value
    mocker.patch("zipfile.ZipFile", new=mock_zip_file)

    Filesystem().extract_project_info(Path("zip"), Path("dest"), "config.ini", with_thumbnails=False)

    assert mock_zip_file.mock_calls[0] == call("zip")
    assert mock_zip_file.mock_calls[2] == call().extract("config.ini", path="dest")


def test_extract_project_info_without_thumbnail_files(mocker):
    mock_zip_file = mocker.MagicMock()
    mock_zip_file.return_value.__enter__.return_value = mock_zip_file.return_value
    mock_zip_file.return_value.namelist.return_value = ["not_thumbnail_file"]
    mocker.patch("zipfile.ZipFile", new=mock_zip_file)

    Filesystem().extract_project_info(Path("zip"), Path("dest"), "config.ini", with_thumbnails=True)

    assert mock_zip_file.mock_calls[0] == call("zip")
    assert mock_zip_file.mock_calls[2] == call().extract("config.ini", path="dest")
    assert mock_zip_file.mock_calls[3] == call().namelist()


def test_extract_project_info_with_thumbnail_files(mocker):
    mock_zip_file = mocker.MagicMock()
    mock_zip_file.return_value.__enter__.return_value = mock_zip_file.return_value
    mock_zip_file.return_value.namelist.return_value = ["thumbnail/yes_file"]
    mocker.patch("zipfile.ZipFile", new=mock_zip_file)

    Filesystem().extract_project_info(Path("zip"), Path("dest"), "config.ini", with_thumbnails=True)

    assert mock_zip_file.mock_calls[0] == call("zip")
    assert mock_zip_file.mock_calls[2] == call().extract("config.ini", path="dest")
    assert mock_zip_file.mock_calls[3] == call().namelist()
    assert mock_zip_file.mock_calls[4] == call().extract("thumbnail/yes_file", path="dest")


# =================================================================================================


# ==========move===================================================================================
def test_move_is_file(mocker):
    normalize_path_mock = mocker.patch(
        "filemanager.filesystem.Filesystem.normalize_path",
        return_value=Path("path"),
    )
    remove_path_mock = mocker.patch("filemanager.filesystem.Filesystem.remove_path")
    mocker.patch("pathlib.Path.is_file", return_value=True)
    copyfile_mock = mocker.patch("shutil.copyfile")

    Filesystem().move(src=Path("source"), dest=Path("destination"))

    normalize_path_mock.assert_has_calls([call(Path("source")), call(Path("destination"))])
    copyfile_mock.assert_called_once_with("path", "path")
    remove_path_mock.assert_called_once_with(Path("path"), recursive=True)


def test_move_mount_file(mocker):
    normalize_path_mock = mocker.patch(
        "filemanager.filesystem.Filesystem.normalize_path",
        return_value=Path("path"),
    )
    remove_path_mock = mocker.patch("filemanager.filesystem.Filesystem.remove_path")
    mocker.patch("pathlib.Path.is_file", return_value=True)
    copyfile_mock = mocker.patch("shutil.copyfile")
    mocker.patch("filemanager.filesystem.Filesystem.is_mount", return_value=True)

    Filesystem().move(src=Path("source"), dest=Path("destination"))

    normalize_path_mock.assert_has_calls([call(Path("source")), call(Path("destination"))])
    copyfile_mock.assert_called_once_with("path", "path")
    remove_path_mock.assert_called_once_with(Path("path"), recursive=True)


def test_move_is_not_file(mocker):
    normalize_path_mock = mocker.patch(
        "filemanager.filesystem.Filesystem.normalize_path",
        return_value=Path("path"),
    )
    remove_path_mock = mocker.patch("filemanager.filesystem.Filesystem.remove_path")
    mocker.patch("pathlib.Path.is_file", return_value=False)
    copyfile_mock = mocker.patch("shutil.copyfile")

    with pytest.raises(TypeError):
        Filesystem().move(src=Path("source"), dest=Path("destination"))

    normalize_path_mock.assert_has_calls([call(Path("source")), call(Path("destination"))])
    copyfile_mock.assert_not_called()
    remove_path_mock.assert_not_called()


# =================================================================================================


# =============================list_files==========================================================
def test_list_files_not_path():
    assert not Filesystem().list_files(123, [".ext1", ".ext2"])  # type: ignore


def test_list_files_all(mocker):
    paths = [
        Path("folder/file"),
        Path("folder/sub/file"),
        Path("folder/sub/sub/file"),
    ]
    for path in paths:
        path.parent.mkdir(exist_ok=True, parents=True)
        open(path, "w", encoding="utf-8")  # pylint: disable=consider-using-with

    mocker.patch(
        "pathlib.Path.rglob",
        return_value=[
            Path("folder"),
            Path("folder/sub"),
            Path("folder/file"),
            Path("folder/sub/file"),
            Path("folder/sub/sub"),
            Path("folder/sub/sub/file"),
        ],
    )

    assert len(Filesystem().list_files(Path(""), [".ext"])) == 6
    rmtree("folder")


def test_list_files_depth(mocker):
    paths = [
        Path("folder/file"),
        Path("folder/sub/file"),
        Path("folder/sub/sub/file"),
    ]
    for path in paths:
        path.parent.mkdir(exist_ok=True, parents=True)
        open(path, "w", encoding="utf-8")  # pylint: disable=consider-using-with

    mocker.patch(
        "pathlib.Path.rglob",
        return_value=[
            Path("folder"),
            Path("folder/sub"),
            Path("folder/file"),
            Path("folder/sub/file"),
            Path("folder/sub/sub"),
            Path("folder/sub/sub/file"),
        ],
    )
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path(""))

    assert len(Filesystem().list_files(Path(""), [".ext"], 2)) == 3
    rmtree("folder")


def test_list_nonexistent_files(mocker):
    mocker.patch(
        "pathlib.Path.rglob",
        return_value=[
            Path("folder"),
            Path("folder/sub"),
            Path("folder/file"),
            Path("folder/sub/file"),
            Path("folder/sub/sub"),
            Path("folder/sub/sub/file"),
        ],
    )
    mocker.patch("filemanager.filesystem.Filesystem.normalize_path", return_value=Path(""))

    assert len(Filesystem().list_files(Path(""), [".ext"], 2)) == 0


# =================================================================================================


# =============================is_valid_path=======================================================
def test_is_valid_path_file_true(mocker):
    mocker.patch("pathlib.Path.is_file", return_value=True)
    check_mock = mocker.patch("typing.Callable", return_value=True)

    assert Filesystem().is_valid_path(path=Path("valid_file"), check=check_mock)


def test_is_valid_path_file_false(mocker):
    mocker.patch("pathlib.Path.is_file", return_value=True)
    check_mock = mocker.patch("typing.Callable", return_value=False)

    assert not Filesystem().is_valid_path(path=Path("invalid_file"), check=check_mock)

    check_mock.assert_called_once_with(Path("invalid_file"))


def test_is_valid_path_hidden(mocker):
    is_file_mock = mocker.patch("pathlib.Path.is_file", return_value=True)
    check_mock = mocker.patch("typing.Callable", return_value=False)

    assert not Filesystem().is_valid_path(path=Path(".hidden"), check=check_mock)

    is_file_mock.assert_not_called()
    check_mock.assert_not_called()


def test_is_valid_path_directory_true(mocker):
    mocker.patch("pathlib.Path.is_file", return_value=False)
    mocker.patch("pathlib.Path.rglob", return_value=[Path("first"), Path("second")])
    check_mock = mocker.patch("typing.Callable", return_value=True)

    assert Filesystem().is_valid_path(path=Path("directory"), check=check_mock)

    check_mock.assert_called_once_with(Path("first"))


def test_is_valid_path_directory_false(mocker):
    mocker.patch("pathlib.Path.is_file", return_value=False)
    mocker.patch("pathlib.Path.rglob", return_value=[Path("first"), Path("second")])
    check_mock = mocker.patch("typing.Callable", return_value=False)

    assert not Filesystem().is_valid_path(path=Path("directory"), check=check_mock)

    check_mock.assert_has_calls([call(Path("first")), call(Path("second"))])


# =================================================================================================


# =============================wait_until==========================================================
def test_wait_until_success():
    assert Filesystem().wait_until(condition=lambda: True)


def test_wait_until_fail():
    assert not Filesystem().wait_until(condition=lambda: False)


# =================================================================================================


# =============================usb_monitor_factory=================================================
def test_usb_monitor_factory(mocker):
    monitor_mock = mocker.MagicMock(spec=pyudev.Monitor)
    observer_mock = mocker.MagicMock(spec=pyudev.MonitorObserver)

    mocker.patch("pyudev.Monitor.from_netlink", return_value=monitor_mock)
    mocker.patch("pyudev.MonitorObserver", return_value=observer_mock)

    assert observer_mock == usb.monitor_factory(callback="callback")


# =================================================================================================


# =============================usb_mounted=========================================================
def test_usb_mounted_false(mocker):
    mocker.patch("pyudev.Context.list_devices", return_value=[])

    assert not usb.inserted(context=pyudev.Context()).stem


def test_usb_mounted_sda(mocker):
    device_mock = mocker.MagicMock(spec=pyudev.Device)
    type(device_mock).device_node = mocker.PropertyMock(return_value="/dev/sda")
    type(device_mock).get = mocker.MagicMock(return_value="usb-storage")

    mocker.patch("pyudev.Context.list_devices", return_value=[device_mock])
    device_path = usb.inserted()
    assert device_path.stem == "sda"


# =================================================================================================


# =============================is_mount============================================================
def test_is_mounted_temp():
    assert not Filesystem().is_mount(path=Path(tempfile.gettempdir()) / "somewhere")


def test_is_mounted_root():
    assert not Filesystem().is_mount(path=Path("/"))


def test_is_mounted_true(mocker):
    mocker.patch("pathlib.Path.is_mount", return_value=True)
    assert Filesystem().is_mount(path=Path("/folder"))


def test_is_mounted_nested_true(mocker):
    mocker.patch("pathlib.Path.is_mount", side_effect=[False, True])
    assert Filesystem().is_mount(path=Path("/folder/nested"))


def test_is_mounted_nested_false(mocker):
    mocker.patch("pathlib.Path.is_mount", return_value=False)
    assert not Filesystem().is_mount(path=Path("/folder/nested"))


# =================================================================================================

# =============================is_mount============================================================


def test_available_storage_no_reserved(mocker):
    mocker.patch("shutil.disk_usage", return_value=(10, 5, 5))
    assert Filesystem().available_storage(path=Path("/folder")) == (10, 5, 5)


def test_available_storage_reserved(mocker):
    mocker.patch("shutil.disk_usage", return_value=(10, 5, 5))
    assert Filesystem().available_storage(path=Path("/folder"), reserved=5) == (10, 5, 0)


def test_available_storage_insuficient_reserve(mocker):
    mocker.patch("shutil.disk_usage", return_value=(10, 5, 5))
    assert Filesystem().available_storage(path=Path("/folder"), reserved=10) == (10, 5, 0)
