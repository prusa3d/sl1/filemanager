# -*- coding: utf-8 -*-
# mypy: ignore-errors
# This file is part of the SLA firmware
# Copyright (C) 2022 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import shutil
from pathlib import Path

from unittest.mock import call
from filemanager.manager import Manager
from filemanager.model import PrinterModel
from filemanager import defines


def test_usb(mocker, usb_filesystem):
    def media_changed(_):
        pass

    def media_present(_):
        pass

    def media_mounted(_):
        pass

    usb_device_mock = mocker.MagicMock(spec="pyudev.Device")
    type(usb_device_mock.return_value).device_node = mocker.PropertyMock(return_value="dev")
    type(usb_device_mock.return_value).action = mocker.PropertyMock(return_value="add")
    type(usb_device_mock.return_value).get = mocker.MagicMock(return_value="ID_FS_TYPE")
    # This should be valid context manager that does nothing
    mocker.patch("filemanager.usb.Blktrace", autospec=True)

    usb_monitor_mock = mocker.MagicMock(spec="pyudev.MonitorObserver")
    mocker.patch("filemanager.usb.monitor_factory", return_value=usb_monitor_mock.return_value)

    Path.is_mount = mocker.MagicMock(return_value=[False, True, False])

    # Watch the signal emitted when current_media_path changes
    media_changed_mock = mocker.MagicMock(spec=media_changed)
    media_present_mock = mocker.MagicMock(spec=media_present)
    media_mounted_mock = mocker.MagicMock(spec=media_mounted)

    filemanager = Manager(PrinterModel.get_model())

    filemanager.current_media_path_changed.connect(media_changed_mock)
    filemanager.media_present_changed.connect(media_present_mock)
    filemanager.media_mounted_changed.connect(media_mounted_mock)

    (usb_filesystem / "dev").mkdir()

    assert len(filemanager.storages) == 2

    filemanager.usb_event_handler(usb_device_mock.return_value)

    # [local, previous prints, usb, raucb]
    assert len(filemanager.storages) == 4

    # Remove USB drive
    type(usb_device_mock.return_value).action = mocker.PropertyMock(return_value="remove")
    filemanager.usb_event_handler(usb_device_mock.return_value)

    # [local, previous prints]
    assert len(filemanager.storages) == 2

    # Check if the signal was emitted - current media path does reflect mounted / unmounted USB drive
    assert media_changed_mock.mock_calls[-2:] == [
        call(str(usb_filesystem / "dev")),
        call(""),
    ]

    # Media is first inserted and then removed
    assert media_present_mock.mock_calls[-2:] == [
        call(True),
        call(False),
    ]

    assert media_mounted_mock.mock_calls[-2:] == [
        call(True),
        call(False),
    ]


def test_one_click(mocker, usb_filesystem):
    def media_changed(_):
        pass

    usb_device_mock = mocker.MagicMock(spec="pyudev.Device")
    type(usb_device_mock.return_value).device_node = mocker.PropertyMock(return_value="dev")
    type(usb_device_mock.return_value).action = mocker.PropertyMock(return_value="add")
    mocker.patch("pyudev.Device.get", return_value="anything")
    mocker.patch("filemanager.usb.Blktrace", autospec=True)

    usb_monitor_mock = mocker.MagicMock(spec="pyudev.MonitorObserver")
    mocker.patch("filemanager.usb.monitor_factory", return_value=usb_monitor_mock.return_value)
    media_changed_mock = mocker.MagicMock(spec=media_changed)
    mocker.patch("filemanager.filesystem.Filesystem.ch_mode_owner")
    one_click_mock = mocker.MagicMock(spec=media_changed)

    filemanager = Manager(PrinterModel.get_model())

    filemanager.current_media_path_changed.connect(media_changed_mock)
    filemanager.one_click_print_file.connect(one_click_mock)
    (usb_filesystem / "dev").mkdir()
    project = next(defines.PROJECT_ROOT_PATH.rglob(f"*{PrinterModel.get_model().extensions[0]}"))
    new_project = Path(shutil.copyfile(str(project), str(usb_filesystem / "dev" / project.name)))

    filemanager.usb_event_handler(usb_device_mock.return_value)

    type(usb_device_mock.return_value).action = mocker.PropertyMock(return_value="remove")
    filemanager.usb_event_handler(usb_device_mock.return_value)

    assert media_changed_mock.mock_calls[-2:] == [
        call(str(usb_filesystem / "dev")),
        call(""),
    ]
    assert one_click_mock.mock_calls[-1:] == [
        call(new_project),
    ]


def test_current_project(mocker):
    mocker.patch("filemanager.filesystem.Filesystem.ch_mode_owner")

    filemanager = Manager(PrinterModel.get_model())

    project = next(defines.PROJECT_ROOT_PATH.rglob(f"*{PrinterModel.get_model().extensions[0]}"))

    filemanager.current_project = Path(project)
    assert filemanager.current_project == defines.PROJECT_SYMLINK / project.name
    filemanager.current_project = None
    assert filemanager.current_project == Path("")


def test_get_project_details(mocker):
    mocker.patch("filemanager.filesystem.Filesystem.ch_mode_owner")
    filemanager = Manager(PrinterModel.get_model())

    project = next(defines.PROJECT_ROOT_PATH.rglob(f"*{PrinterModel.get_model().extensions[0]}"))

    result = filemanager.item_details(project, with_metadata=True)
    assert result["path"] == str(project)
    assert result["size"] == project.stat().st_size
    assert result["origin"] == "local"
    assert result["mtime"] == max(project.stat().st_mtime, project.stat().st_ctime)
    assert result["metadata"]["config"]
    assert result["metadata"]["thumbnail"]
