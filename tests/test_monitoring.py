# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2022 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import shutil
from pathlib import Path

from unittest.mock import call

from PySignal import Signal

from filemanager.storage import Storage
from filemanager.cache import Cache
from filemanager import defines
from filemanager.model import PrinterModel
from filemanager.filesystem import Filesystem


def test_create_and_delete(mocker, root_filesystem):
    def handler(_):
        pass

    even_mock = mocker.MagicMock(spec=handler, side_effect=handler)
    mocker.patch("filemanager.filesystem.Filesystem.ch_mode_owner")
    mocker.patch("filemanager.cache.time", side_effect=[1, 2, 3, 4])

    updated = Signal()
    updated.connect(even_mock)

    fsm = Filesystem()
    cache = Cache("local", root_filesystem, PrinterModel.get_model().extensions, fsm, updated)
    storage = Storage(cache)
    last_update = cache.last_update
    project = next(defines.PROJECT_ROOT_PATH.rglob(cache.pattern_list[0]))
    new_project = Path(shutil.copyfile(str(project), str(root_filesystem / project.name)))
    storage.cache.get(new_project)
    fsm.wait_until(lambda: even_mock.mock_calls[-1] == call(3), timeout=10)

    even_mock.assert_called_once_with(3)
    assert last_update < cache.last_update
    last_update = cache.last_update

    new_project.unlink()

    fsm.wait_until(lambda: even_mock.mock_calls[-2:] == [call(3), call(4)], timeout=10)
    assert even_mock.mock_calls[-2:] == [call(3), call(4)]
    assert last_update < cache.last_update
