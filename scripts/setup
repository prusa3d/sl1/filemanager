#!/bin/bash

# This file is part of the SLA firmware
# Copyright (C) 2020-2023 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

VENV_PATH="$(pwd)/.venv"
GIT_HOOK="$(git rev-parse --git-common-dir)/hooks/pre-commit"
STATIC_TEST_SCRIPT="$(pwd)/scripts/static_test"
TEST_SCRIPT="$(pwd)/scripts/test"

python3 --version

# Creating virtual environments
python3 -m venv "$VENV_PATH"

add_pre_commit()
{
cat > "$GIT_HOOK" << EOF
#!/bin/bash

set +e

${STATIC_TEST_SCRIPT} || error=true
${TEST_SCRIPT} -p sl1 -n || error=true
${TEST_SCRIPT} -p sl1s -n || error=true
${TEST_SCRIPT} -p m1 -n || error=true

if [ \$error ]
then
    exit 1
fi
EOF
chmod +x "$GIT_HOOK"
}

echo "updating dependencies"
git submodule update --init --recursive

echo "activating venv"
source "$VENV_PATH/bin/activate"

# install 3th party
echo "installing requirements"
pip install -r ./requirements.txt

# pre-commit install
echo "installing pre commit hooks"
python -m pre_commit install-hooks --config=.pre-commit-config.yaml

if [ -z "$CI" ]
then
    if [ -f "$GIT_HOOK" ]
    then
        mv "$GIT_HOOK" "$GIT_HOOK.legacy"
        echo "Running in migration mode with existing hooks at $GIT_HOOK.legacy"
    fi
    add_pre_commit
fi
